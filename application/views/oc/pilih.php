<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <?php $this->load->view('admin/head.php') ?>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css" /> -->
    <script src="<?php echo base_url(); ?>assets/js/modernizr.js"></script>
    <script>
        if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            var root = document.getElementsByTagName('html')[0];
            root.setAttribute('class', 'ff');
        }
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/greensock/minified/TweenMax.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/greensock/minified/plugins/DrawSVGPlugin.min.js"></script>
    <style>
        span {
            float: left;
        }

        h3 {
            text-align: left;
            width: 173px;
        }

        .cs-style-4 figcaption button {
            position: absolute;
            bottom: 20px;
            right: 20px;
        }

        .grid figcaption button {
            text-align: center;
            padding: 5px 10px;
            border-radius: 0px;
            border: none;
            display: inline-block;
            background-color: #696969;
            color: #fff;
        }

        body {
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>

</head>

<body style="background-image: url('<?php echo base_url(); ?>/assets/img/bg8.jpg') no-repeat center center fixed; ">

    <!-- <div id="pb_loader" class="show fullscreen">
            <svg id="loader" width="100%" height="100%" viewBox="0 0 200 200" preserveAspectRatio="xMidYMid meet">
                <path id="jump" fill="none" stroke="#383845" stroke-width="10" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M47.5,94.3c0-23.5,19.9-42.5,44.5-42.5s44.5,19,44.5,42.5" />
                <g stroke="#383845" stroke-width="1">
                    <ellipse id="circleL" fill="none" stroke-miterlimit="10" cx="47.2" cy="95.6" rx="10.7" ry="2.7" />
                    <ellipse id="circleR" fill="none" stroke-miterlimit="10" cx="136.2" cy="95.6" rx="10.7" ry="2.7" />
                </g>
            </svg>
        </div> -->

    <div class="ip-container" id="ip-container">
        <?php $this->load->view('assets/nav'); ?>
        <div class="header">
            <h5 class="text-warning">
                <center>Selamat Datang <?php
                                        $nim = strtoupper($this->session->userdata['user_data']['idpemilih']);
                                        echo $nim;
                                        ?> <br />
                    Silahkan Pilih Sesuai Hati Nurani Anda</center>
            </h5>
        </div>
        <div class="content-wrap">
            <div class="content">
                <!-- <div class="ip-main"> -->
                <ul class="grid cs-style-4" style="padding: 0 0 0 0;align-content: center;">
                    <?php
                    //var_dump($calon);
                    if (!empty($calon)) {
                        foreach ($calon as $c) {
                            ?>
                            <li style="width:420px">
                                <figure>
                                    <div><img src="<?php echo base_url(); ?>assets/upload/<?= $c['foto']; ?>" width="300px" height="400px" alt="Image" /></div>
                                    <figcaption>
                                        <span>Calon Ketua Nomer Urut <?= $c['kdpaslon']; ?> </span>
                                        <p>
                                            <h3> <?= $c['nmketua']; ?></h3> <br>
                                            <h3> <?= $c['nmwakil']; ?></h3>
                                        </p>
                                        <div id="la-buttons">
                                            <button data-anim="la-anim-10" onclick="javascript:getValue(this.value)" value="<?= $c['kdpaslon']; ?>" style="background-color: tomato; width: 100px; margin: 0 auto; color: white;"> Vote </button>
                                            <!--<a href="javascript:void(0)" data-anim="la-anim-10"  style="background-color: tomato; width: 100px; margin: 0 auto; color: white;"> Vote</a>-->
                                        </div>
                                    </figcaption>
                                </figure>
                            </li>
                    <?php
                        }
                    }
                    ?>
                </ul>
                <!-- </div> -->
            </div>
        </div><!-- /content-wrap -->

    </div>
    <div class="la-anim-10"></div>

    <audio style="overflow: hidden;visibility: hidden;" id="beep-one" controls="controls" preload="auto">
        <source src="<?php //echo base_url(); 
                        ?>assets/audio/false.wav">
        </source>
        Your browser isn't invited for super fun time.
    </audio>
    <footer class="footer">
        <?php $this->load->view('footer'); ?>
    </footer>
</body>

</html>
<?php $this->load->view('assets/js.php') ?>
<script>
    var id;

    function getValue(e) {
        id = e;
        setAudio('<?php echo base_url(); ?>assets/audio/true.wav');
        return id;
        //        var cek = $.post('<?php echo base_url(); ?>pilih/proses', {name: id});
        //        console.log(cek);
    }

    var loader = document.getElementById('la-anim-6-loader'),
        border = document.getElementById('la-anim-6-border'),
        α = 0,
        π = Math.PI,
        t = 15,
        tdraw;
    var inProgress = false;
    Array.prototype.slice.call(document.querySelectorAll('#la-buttons > button')).forEach(function(el, i) {
        //                var n = $('#no').val();
        var anim = el.getAttribute('data-anim'),
            animEl = document.querySelector('.' + anim);
        el.addEventListener('click', function() {
            if (inProgress)
                return false;
            inProgress = true;
            classie.add(animEl, 'la-animate');
            setTimeout(function() {
                classie.remove(animEl, 'la-animate');
                inProgress = false;

                if (inProgress === false) {
                    var url = '<?= base_url() ?>pilih/proses/?id=' + id;

                    setTimeout("window.open( '" + url + "' , '_self')", 900);
                    window.location.href = '<?= base_url() ?>pilih/proses/?id=' + id;
                }

            }, 3000);

        });
    });
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>


<script src="<?php echo base_url(); ?>assets/js/loader.js"></script>

<script>
    (function() {
        var container = document.getElementById('container');
        var loader = document.getElementById('loader');
        var circleL = document.getElementById('circleL');
        var circleR = document.getElementById('circleR');
        var jump = document.getElementById('jump');
        var jumpRef = jump.cloneNode();

        loader.appendChild(jumpRef);

        TweenMax.set([container, loader], {
            position: 'absolute',
            top: '50%',
            left: '50%',
            xPercent: -50,
            yPercent: -50
        })

        TweenMax.set(jumpRef, {
            transformOrigin: '50% 110%',
            scaleY: -1,
            alpha: 0.05
        })

        var tl = new TimelineMax({
            repeat: -1,
            yoyo: false
        });

        tl.timeScale(3);

        tl.set([jump, jumpRef], {
                drawSVG: '0% 0%'
            })
            .set([circleL, circleR], {
                attr: {
                    rx: 0,
                    ry: 0,
                }
            })
            .to([jump, jumpRef], 0.4, {
                drawSVG: '0% 30%',
                ease: Linear.easeNone
            })
            .to(circleL, 2, {
                attr: {
                    rx: '+=30',
                    ry: '+=10'
                },
                alpha: 0,
                ease: Power1.easeOut
            }, '-=0.1')
            .to([jump, jumpRef], 1, {
                drawSVG: '50% 80%',
                ease: Linear.easeNone
            }, '-=1.9')
            .to([jump, jumpRef], 0.7, {
                drawSVG: '100% 100%',
                ease: Linear.easeNone
            }, '-=0.9')
            .to(circleR, 2, {
                attr: {
                    rx: '+=30',
                    ry: '+=10'
                },
                alpha: 0,
                ease: Power1.easeOut
            }, '-=.5')
    })();
</script>