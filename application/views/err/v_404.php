
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Kesasar</title>
        <link rel="shortcut icon" href="<?= base_url(); ?>assets/img/logo.png" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/now-ui-kit.css" />-->
        <?php $this->load->view('admin/head.php') ?>  
    </head>
    <body>




        <div id="container">
            <section class="content">
                <div class="error-page" style="margin-top: 100px">
                    <center>
                        <p class="text-danger" style="font-size: 200px;"> <i class="fa fa-warning text-yellow"></i>  404  </p>
                    </center>
                </div>
                <!-- /.error-page -->



            </section>
            <section class="content" style="padding-top: 10px;">
                <div class="error-content text-center">
                    <h3> Kesasar! Ngapuntene Mas.</h3>
                    <p>
                        Halaman ingkang panjenengan pados mboten wonten.<br>
                        Mbalik <a href="<?php echo base_url('home'); ?>">halaman utama</a>
                    </p>
                </div>
            </section>
        </div>
    </body>
    <footer class="footer">
        <?php
        $this->load->view('footer.php');
        ?>
    </footer>

</html>