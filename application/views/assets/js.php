<script src="<?php echo base_url(); ?>assets/js/classie.js"></script>
<script src="<?php echo base_url(); ?>assets/plugin/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>assets/plugin/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/plugin/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/js/svgLoader.js"></script>
<script src="<?php echo base_url(); ?>assets/js/pathLoader.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ma.js"></script>
<script src="<?php echo base_url(); ?>assets/js/toucheffects.js"></script>
<script src="<?php echo base_url(); ?>assets/js/classie.js"></script>
<script src="<?php echo base_url(); ?>assets/plugin/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/menu.js"></script>
<script src="<?php echo base_url(); ?>assets/plugin/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugin/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugin/datatables.net/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugin/datatables.net-bs/js/dataTables.bootstrap.js"></script>
<script src="<?= base_url() ?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/buttons.flash.min.js"></script>
<script src="<?= base_url() ?>assets/js/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.PrintArea.js" type="text/JavaScript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js" integrity="sha256-qSIshlknROr4J8GMHRlW3fGKrPki733tLq+qeMCR05Q=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js" integrity="sha256-xKeoJ50pzbUGkpQxDYHD7o7hxe0LaOGeguUidbq6vis=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js" integrity="sha256-arMsf+3JJK2LoTGqxfnuJPFTU4hAK57MtIPdFpiHXOU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" integrity="sha256-Uv9BNBucvCPipKQ2NS9wYpJmi8DTOEfTA/nH2aoJALw=" crossorigin="anonymous"></script>
<script>
    var beepOne = $("#beep-one")[0];
    var change;
    function setAudio(audio) {
        $(beepOne).attr("src", audio);
        beepOne.play();
    }

    $(function () {
        $('[data-mask]').inputmask();
    });
</script>
