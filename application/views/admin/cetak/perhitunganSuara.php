<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?= $title ?></title>
        <link href="<?php echo base_url(); ?>assets/img/bakaranproject.png" rel="shortcut icon" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/tabel.css" >
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="keywords" content="One Click Voting System" />
        <meta name="author" content="Willy" />
        <style>
            body{
                font-size: 14px;
            }
            body, h2 {
                font-family: "Courier new";
            }
            h2,h3{
                margin-bottom: 5px;
                margin-top: 0px;
            }
            .garis{
                height: 2px;
                background-color: #000;
                margin-bottom: 0px;
                margin-top: 10px;
            }
            hr{
                height: 0.5px;
                background-color: #000;
                margin-top: 5px;
            }
            p{
                margin: 0 0 0 0;
                padding: 0 0 0 0;
            }
            .left{text-align: left}
            .right{text-align: right}
            .center{text-align: center}

        </style>

    </head>

    <body style="font-family: 'Courier';">
        <div class="container">
            <div align="center" style="margin-top: 10px;">
                <h2 align="center" style="font-family: 'Courier';">HASIL PERHITUNGAN SUARA PEMILIHAN KETUA UMUM BAKARAN PROJECT</h2>
                <h2 align="center" style="font-family: 'Courier';">PERIODE 2018/2019</h2>
                <h3 align="center">LAPORAN PER <?= strtoupper(date('M Y')); ?></h3>
                <hr class="garis"/>
                <hr/>
            </div>
            <br/>
            <div class="right" align='right' style="margin-top: 0px;"><b><p style="text-align: right;"><?= $tgl; ?></p></b></div>
            <br/>
            <table align="center" class="table table-striped table-hover" style=" vertical-align: central; margin-top: 20px;">
                <thead>
                    <tr>
                        <th style=" border-color: #000;">Kode Calon</th>
                        <th style=" border-color: #000;">Nama Ketua</th>
                        <th style=" border-color: #000;">Nama Wakil</th>
                        <th style=" border-color: #000;">Jumlah Suara</th>
                        <th style=" border-color: #000;">Prosentase</th>
                    </tr>
                </thead>
                <?php
                $total = doubleval($totalsuara->total);
                if (!empty($paslon)) {
                    foreach ($paslon as $value) {
                        $suara = $this->Laporan->sum('jmlsuara', 'kdpaslon', $value->kdpaslon)->row();
                        $prosentase = 0;
                        if (doubleval($suara->total) != 0) {
                            $prosentase = (sprintf("%2.2f", doubleval($suara->total) / $totalterdaftar) * 100);
                        }
                        if ($suara->total == "") {
                            $suara->total = 0;
                        }
                        ?>
                        <tr>
                            <th><p class="center"><?= $value->kdpaslon; ?></p></th>
                            <th><?= $value->nmketua; ?></th>
                            <th><?= $value->nmwakil; ?></th>
                            <th><b><p class="center"><?= $suara->total; ?></p></b></th>
                            <th><b><p class="center"><?= $prosentase ?> %</p></b></th>
                        </tr>
                        <?php
                    }
                }
                ?>    
                <tfoot>
                    <tr>
                        <th style=" border-color: #000;" colspan="3">Total Pemilih</th>
                        <th style=" border-color: #000;"><p class="center"><?= $total ?></p></th>
                        <th style=" border-color: #000;"><p class="center"><?= $total / $totalterdaftar * 100 ?> %</p></th>
                    </tr>
                    <tr>
                        <th colspan="3">Jumlah Abstain</th>
                        <th><p class="center"><?= $totalterdaftar - $total ?></p></th>
                        <th><p class="center"><?= 100 - ($total / $totalterdaftar * 100) ?> %</p></th>
                    </tr>
                    <tr>
                        <th colspan="3">Total Pemilih Terdaftar</th>
                        <th><p class="center"><?= $totalterdaftar ?></p></th>
                        <th><p class="center"><?= $totalterdaftar / $totalterdaftar * 100 ?> %</p></th>
                    </tr>
                </tfoot>
            </table>
            <br/>
            <table style=" width: 100%;
                   max-width: 100%; margin-top: 20px;">
                <tr>
                    <td><p align="center" style="margin-bottom: 75px;"><b>Pembina Bakaran Project</b></p></td>
                    <td><p align="center" style="margin-bottom: 75px;"><strong>Ketua Bakaran Project</strong></p></td>
                </tr>
                <tr>
                    <td><p align="center" style="margin-bottom: 0px;"><strong>Yoga Willy Utomo</strong></p></td>
                    <td><p align="center" style="margin-bottom: 0px;"><strong>Yoga Willy Utomo</strong></p></td>
                </tr>
                <hr/>
            </table>
        </div>	
    </body>
</html>










