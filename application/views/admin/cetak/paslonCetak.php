<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?= $title ?></title>
        <link href="<?php echo base_url(); ?>assets/img/bakaranproject.png" rel="shortcut icon" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/tabel.css" >
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="keywords" content="One Click Voting System" />
        <meta name="author" content="Willy" />
        <style>
            /*            @font-face {
                            font-family: 'Courier new';
                            font-weight: normal;
                            font-style: normal;
                            font-variant: normal;
                            src: url('<?php echo base_url() ?>assets/fonts/glyphicons-halflings-regular.ttf'); 
                        }*/
            body, h2 {
                font-family: "Courier new";
            }
            h2,h3{
                margin-bottom: 5px;
                margin-top: 0px;
            }
            .garis{
                height: 2px;
                background-color: #000;
                margin-bottom: 0px;
                margin-top: 10px;
            }
            hr{
                height: 0.5px;
                background-color: #000;
                margin-top: 5px;
            }

        </style>

    </head>

    <body style="font-family: 'Courier';">
        <div class="container">
            <div align="center" style="margin-top: 10px;">
                <h2 align="center" style="font-family: 'Courier';">DATA PASANGAN CALON KETUA UMUM BAKARAN PROJECT</h2>
                <h2 align="center" style="font-family: 'Courier';">PERIODE 2018/2019</h2>
                <h3 align="center">LAPORAN PER <?= strtoupper(date('M Y')); ?></h3>
                <hr class="garis"/>
                <hr/>
            </div>
            <div class="right" align='right' style="margin-top: 0px;"><p><?= $tgl; ?></p></div>
            <table align="center" class="table table-bordered table-striped table-hover" style="width: 900px; vertical-align: central; margin-top: 20px;">
                <thead>
                <th>Kode Calon</th>
                <th>Nama Ketua</th>
                <th>Nama Wakil</th>
                <th>Motto</th>
                <th>Tanggal Daftar</th>
                <th>Foto</th>
                </thead>
                <?php
                if (!empty($paslon)) {
                    foreach ($paslon as $value) {
                        $gambar = $value->foto;
                        if ($gambar == "" || $gambar == NULL) {
                            $gambar = "[No Photo]";
                        } else {
                            $gambar = base_url() . "assets/upload/" . $value->foto;
                            $gambar = "<img src=' " . $gambar . " ' style='width: 100px;'/>";
                        }
                        ?>
                        <tr>
                            <td><?= $value->kdpaslon; ?></td>
                            <td><?= $value->nmketua; ?></td>
                            <td><?= $value->nmwakil; ?></td>
                            <td><?= $value->motto; ?></td>
                            <td><?= $value->tgldaftar; ?></td>
                            <td><?= $gambar ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>            
            </table>
            <table style=" width: 100%;
                   max-width: 100%;">
                <tr>
                    <td><p align="center" style="margin-bottom: 50px;"><b>Pembina Bakaran Project</b></p></td>
                    <td><p align="center" style="margin-bottom: 50px;"><strong>Ketua Bakaran Project</strong></p></td>
                </tr>
                <tr>
                    <td><p align="center" style="margin-bottom: 0px;"><strong>Yoga Willy Utomo</strong></p></td>
                    <td><p align="center" style="margin-bottom: 0px;"><strong>Yoga Willy Utomo</strong></p></td>
                </tr>
            </table>


        </div>	

        <script>
            window.print();
        </script>
    </body>
</html>










