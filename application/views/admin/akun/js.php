
<?php $this->load->view('assets/js') ?>
<script>
    var polyfilter_scriptpath = '/js/';
</script>
<!--<script src="<?php echo base_url(); ?>assets/js/cssParser.js"></script>-->
<!--<script src="<?php echo base_url(); ?>assets/js/css-filters-polyfill.js"></script>-->
<script src="<?php echo base_url(); ?>assets/js/selectFx.js"></script>
<script src="<?php echo base_url(); ?>assets/js/modalEffects.js"></script>
<script src="<?php echo base_url(); ?>assets/js/selectFx.js"></script>
<script>

    function GeneratePassword(lengthOfPassword) {
        var theLetters = "abcdefghijklmnopqrstuvwxyz!@$&/()_-=?<>";
        var StrongPasswordArray = [];
        var capitalise;
        for (var i = 0; i < lengthOfPassword; i++) {
            capitalise = Math.round(Math.random() * 1);
            if (capitalise === 0) {
                StrongPasswordArray[i] = theLetters.charAt(Math.round(Math.random() * 40)).toUpperCase();
            } else {
                StrongPasswordArray[i] = theLetters.charAt(Math.round(Math.random() * 35));
            }
        }
        var numberOfDigits;
        numberOfDigits = Math.round(Math.random() * (lengthOfPassword - 1)) + 1;
        var positionForNumeric, theNumber;
        for (i = 0; i < numberOfDigits; i++) {
            positionForNumeric = Math.round(Math.random() * (lengthOfPassword - 1));
            theNumber = Math.round(Math.random() * 9);
            StrongPasswordArray[positionForNumeric] = theNumber;
        }

        return StrongPasswordArray;
    }

    function newPassword() {
        var pass = GeneratePassword(16);
    }

    (function () {
        [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
            new SelectFx(el);
        });
    })();

    function searchFilter(page_num) {
        page_num = page_num ? page_num : 0;
        var keywords = $('#keywords').val();
        var sortBy = $('#sortBy').val();
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url(); ?>Admin/paginationData/' + page_num,
            data: 'page=' + page_num + '&keywords=' + keywords + '&sortBy=' + sortBy,
            beforeSend: function () {
                $('.loading').show();
            },
            success: function (data) {
                $('#postList').html(data);
                $('.loading').fadeOut("slow");
            }
        });
    }

    function simpan() {

        $('#tambah').attr('disabled', true);
        var username = $('#username').val();
        var otoritas = $('#otoritas').val();
        if (username == "" || otoritas == "" || username == null || otoritas == null) {
            setAudio('<?php echo base_url(); ?>assets/audio/false.wav');
            swal('Mohon Lengkapi Data');
            $('#tambah').attr('disabled', false);
            return false;
        } else {
            $.post('<?php echo base_url(); ?>Admin/cekUserName', {username: username}, function (d) {
                if (d < 1) {
                    var formData = new FormData($('#frm_login')[0]);
                    $.ajax({
                        url: '<?php echo base_url(); ?>Admin/tambahAdmin',
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: "JSON",
                        success: function (data)
                        {
//                            alert(data.status);
                            if (data.status)
                            {

                                setAudio('<?php echo base_url(); ?>assets/audio/true.wav');
                                swal('Berhasil menambahkan ' + username + " sebagai " + otoritas.toString().toUpperCase());
                                $('#username').val("");
                                searchFilter();
                                $('#tambah-admin').hide();
//                                                $('#otoritas option:first').prop('selected', true);
                            } else
                            {
                                for (var i = 0; i < data.inputerror.length; i++)
                                {
                                    swal(data.error_string[i]);
                                }
                            }
                            return true;
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data' + jqXHR.getAllResponseHeaders() + textStatus + errorThrown);
                        }
                    });

                } else {
                    setAudio('<?php echo base_url(); ?>assets/audio/false.wav');
                    swal(username + ' Sudah Terdaftar');
                    return false;
                }
                $('#tambah').attr('disabled', false);
            });
        }

    }

    function hapus(id, username) {
        setAudio('<?php echo base_url(); ?>assets/audio/false.wav');
        swal({
            title: "Hapus Data !!!",
            text: "Yakin akan menghapus data " + username + "?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            closeOnConfirm: true
        },
                function () {
                    $.ajax({
                        url: "<?php echo site_url('Admin/deleteAdmin') ?>",
                        type: "POST",
                        data: {id: id},
                        dataType: "JSON",
                        success: function (data)
                        {
                            swal('Berhasil Menghapus ' + username);
                            setAudio('<?php echo base_url(); ?>assets/audio/true.wav');

                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error deleting data' + jqXHR.getAllResponseHeaders() + textStatus + errorThrown);
                        }

                    });
                    searchFilter();
                }
        );
    }

    function changeStatus(id, data, username) {
        var s, c;
        if (data == "active") {
            s = "Mengaktifan";
            c = "Activate";
        } else {
            s = "Me-Nonaktifkan";
            c = "Deactivate";
        }

        setAudio('<?php echo base_url(); ?>assets/audio/false.wav');
        swal({
            title: "Peringatan !!!",
            text: "Yakin akan " + s + " " + username + "?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: c,
            closeOnConfirm: true
        },
                function () {
                    $.post('<?php echo base_url(); ?>Admin/changeStatus', {id: id, status: data}, function (z) {
                        if (z == 1)
                        {
                            swal("Berhasil " + s + " " + username);
                        } else {
                            swal("Terjadi Kesalahan");
                        }
                        searchFilter();
                    });
                }
        );
    }
    
    $(document).ready(function (){
//        $('#admin tr:odd').addClass('zebra');
    });


</script>
