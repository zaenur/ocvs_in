<table id="admin" class="table table-bordered table-responsive table-hover border center">
    <thead>
    <th>No</th>
    <th>Username</th>
    <th>Otoritas</th>
    <th colspan="2">Aksi</th>
</thead>
<?php
$no = 0;
if (!empty($posts)): foreach ($posts as $post):
        ?>
        <?= ($no & 1 ? "<tr class='zebra'>" : "<tr>") ?>
        <td class="list-item"><?= ++$no; ?></td> 
        <td class="list-item"><?= $post['username']; ?></td> 
        <td class="list-item"><?= $post['otoritas']; ?></td> 
        <td class="list-item">
            <a href="javascript:void(0);" onclick="hapus('<?= $post['kdadmin']; ?>', '<?= $post['username']; ?>');"><i class="fa fa-trash-o"></i> Delete </a>
            <?php
            if ($post['status'] == "active") {
                ?>
                <a href="javascript:void(0);" onclick="changeStatus('<?= $post['kdadmin']; ?>', 'deactive', '<?= $post['username']; ?>');"><i class="fa fa-unlock"></i> Deactivate</a>
                <?php
            } else {
                ?>
                <a href="javascript:void(0);" onclick="changeStatus('<?= $post['kdadmin']; ?>', 'active', '<?= $post['username']; ?>');"><i class="fa fa-lock"></i> Activate</a>
                <?php
            }
            ?>
        </td>
        </tr>                                   
        <?php
    endforeach;
else:
    ?>
    <dt>Data not available.</dt>
<?php endif; ?>
</table>
<p><?php echo $this->ajax_pagination->create_links(); ?></p>