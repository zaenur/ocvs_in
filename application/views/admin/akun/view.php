<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <?php $this->load->view('admin/head.php'); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/modal.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cs-select.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cs-skin-elastic.css" />
        <style> 
            .row{position: relative; margin-top: 20px; padding-top: 10px; border-radius: 10px; background-color: #b4bad2; margin-left: 10px; margin-right: 10px;}
            p{color: #ffffff;}
            label{letter-spacing: 0.5rem;}
            .container{
                background-color: transparent;
                margin-top: 10px;
            }
            h3{
                color: #000000;
                text-align: center;
            }
            #form{
                margin: auto;
                max-width: 550px;
                /*max-height: 350px;*/
                border-radius: 15px;
                background: #cccccc;
                border: 5px black solid;
                /*margin-top: 200px;*/
            }
            .table{
                align-content: center; max-width: 600px; 
            }
            .zebra{
                background-color: #000080; color: #666;
            }
        </style>
    </head>
    <body>
        <div class="ip-container" id="ip-container">
            <?php $this->load->view('admin/menu/view'); ?>
            <div id="form" class="content-wrap" align="center" style="margin-top: 130px;">
                <h2>Admin OVCS</h2>
                <div class="row">
                    <div class="search col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px; margin-right: 3px;">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <input type="text" class="form-control" id="keywords" placeholder="Cari" onkeyup="searchFilter()"/>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <select id="sortBy" class="form-control" onchange="searchFilter()">
                                <option value="">Sort</option>
                                <option value="asc">Ascending</option>
                                <option value="desc">Descending</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="postList">
                        <table id="admin" class="table table-bordered table-responsive table-hover border center">
                            <thead>
                            <th>No</th>
                            <th>Username</th>
                            <th>Otoritas</th>
                            <th colspan="2">Aksi</th>
                            </thead>
                            <?php
                            $no = 0;
                            if (!empty($posts)): foreach ($posts as $post):
                                    ?>
                                    <?= ($no & 1 ? "<tr class='zebra'>" : "<tr>") ?>
                                    <td class="list-item"><?= ++$no; ?></td> 
                                    <td class="list-item"><?= $post['username']; ?></td> 
                                    <td class="list-item"><?= $post['otoritas']; ?></td> 
                                    <td class="list-item">
                                        <a href="javascript:void(0);" onclick="hapus('<?= $post['kdadmin']; ?>', '<?= $post['username']; ?>');"><i class="fa fa-trash-o"></i> Delete </a>
                                        <?php
                                        if ($post['status'] == "active") {
                                            ?>
                                            <a href="javascript:void(0);" onclick="changeStatus('<?= $post['kdadmin']; ?>', 'deactive', '<?= $post['username']; ?>');"><i class="fa fa-unlock"></i> Deactivate</a>
                                            <?php
                                        } else {
                                            ?>
                                            <a href="javascript:void(0);" onclick="changeStatus('<?= $post['kdadmin']; ?>', 'active', '<?= $post['username']; ?>');"><i class="fa fa-lock"></i> Activate</a>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    </tr>                                   
                                    <?php
                                endforeach;
                            else:
                                ?>
                                <dt>Data not available.</dt>
                            <?php endif; ?>
                        </table>
                        <p><?= $this->ajax_pagination->create_links(); ?></p>
                    </div>
                    <div class="loading" style="display: none;"><div class="content"><img src="<?php echo base_url() . 'assets/images/loading.gif'; ?>"/></div></div>

                </div>
            </div>
            <div class="container">
                <center><button class="btn btn-success btn-round md-trigger md-setperspective" onclick="$('#tambah-admin').show();" data-modal="tambah-admin"><i class="glyphicon glyphicon-user"></i> Tambah Admin</button></center>
            </div>
        </div>




        <footer class="footer">
            <?php $this->load->view('footer'); ?>
        </footer>
    </body>
    <div class="md-overlay"></div>
    <div class="md-modal md-effect-4" id="tambah-admin" role="dialog">
        <div class="md-content" id="form" align=""center>
            <h3><b>Tambah Admin</b></h3>
            <div>
                <form action="javascript:void(0);" id="frm_login" method="POST" autocomplete="off" align="center" style="background-color: #b4bad2;">
                    <section class="content">
                        <span class="input input--kozakura">
                            <input class="input__field input__field--kozakura" type="text" id="username" name="username"/>
                            <label class="input__label input__label--kozakura">
                                <span class="input__label-content input__label-content--kozakura">Username</span>
                            </label>
                            <svg class="graphic graphic--kozakura" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M1200,9c0,0-305.005,0-401.001,0C733,9,675.327,4.969,598,4.969C514.994,4.969,449.336,9,400.333,9C299.666,9,0,9,0,9v43c0,0,299.666,0,400.333,0c49.002,0,114.66,3.484,197.667,3.484c77.327,0,135-3.484,200.999-3.484C894.995,52,1200,52,1200,52V9z"/>
                            </svg>
                        </span>
                        <span class="input" style="margin-bottom: 0px; z-index: 6; position: relative;">
                            <select class="cs-select cs-skin-elastic" id="otoritas" name="otoritas">
                                <option value="" disabled selected>Pilih Otoritas</option>
                                <option value="super admin">Super Admin</option>
                                <option value="admin">Admin</option>
                            </select>
                        </span>
                        <span class="input input--kozakura" style="margin-top: -20px;">
                            <input id="tambah" class="action" onclick="simpan()" type="submit" name="submit" id="submit" value="Tambah"/>
                        </span>

                    </section>
                </form>
                <h1 id="pass" class="center text-danger text-center"></h1>
                <button class="btn btn-danger btn-round btn-lg md-close">Cancel</button>
            </div>
        </div>
    </div>
</html>
<?php $this->load->view('admin/akun/js') ?>
