<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.js"></script>
<?php $this->load->view('assets/js.php') ?>
<script src="<?php echo base_url(); ?>assets/js/notificationFx.js"></script>
<script type="text/javascript">
    function notif(message) {
        var notification = new NotificationFx({
            message: '<span class="icon"></span><center><p> ' + message + ' </p></center>',
            layout: 'bar',
            effect: 'slidetop',
            type: 'warning'
        });
        notification.show();
    }

    $(function () {
        $('#frm_login').submit(function () {
            var username = document.forms["frm_login"]["username"].value;
            var pass = $('#pass').val();
            if (username === "" && pass === "") {
                notif("Maaf, Mohon Untuk Mengisi Username Dan Password Anda");
                setAudio('<?php echo base_url(); ?>assets/audio/false.wav');
                return false;
            } else if (username === "") {
                notif("Maaf, Mohon Untuk Mengisi Username Anda");
                setAudio('<?php echo base_url(); ?>assets/audio/false.wav');
                return false;
            } else if (pass === "") {
                notif("Maaf, Mohon Untuk Mengisi Password Anda");
                setAudio('<?php echo base_url(); ?>assets/audio/false.wav');
                return false;
            } else {
                $.post('<?php echo base_url(); ?>Admin/cekUser', {username: username}, function (d) {
                    if (d == 1) {
                        $.post('<?php echo base_url(); ?>Admin/cekPassword', {username: username, pass: pass}, function (p) {
//                            alert(p);
                            if (p != 0) {
                                $('#login-info').removeClass().addClass('last').html(p);
                               notif("Terima Kasih Telah Menggunakan Layanan Bakaran Project");
                                setAudio('<?php echo base_url(); ?>assets/audio/true.wav');
                                setTimeout("window.open(self.location, '_self');", 3000);
                                return true;
                            } else {
                                notif("Maaf, Password Anda Salah Atau Akun Anda Di Blokir");
                                setAudio('<?php echo base_url(); ?>assets/audio/false.wav');
                                return false;
                            }

                        });
                    } else {
                        notif("Maaf, Username Anda belum Terdaftar");
                        setAudio('<?php echo base_url(); ?>assets/audio/false.wav');
                        return false;
                    }
                });
                return false;
            }
        });
    });
</script>
