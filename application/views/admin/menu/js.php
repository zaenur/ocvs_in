<script src="<?php echo base_url(); ?>assets/js/dialogFx.js"></script>
<script>

    $(document).ready(function () {
        var oldpassword = $('#oldpassword');
        var password1 = $('#password1'); //id of first password field
        var password2 = $('#password2'); //id of second password field
        var passwordsInfo = $('#pass-info'); //id of indicator element
        passwordStrengthCheck(oldpassword, password1, password2, passwordsInfo); //call password check function
    });
    (function () {
        var dlgtrigger = document.querySelector('[data-dialog]'),
                somedialog = document.getElementById(dlgtrigger.getAttribute('data-dialog')),
                // svg..
                morphEl = somedialog.querySelector('.morph-shape'),
                s = Snap(morphEl.querySelector('svg')),
                path = s.select('path'),
                steps = {
                    open: morphEl.getAttribute('data-morph-open'),
                    close: morphEl.getAttribute('data-morph-close')
                },
                dlg = new DialogFx(somedialog, {
                    onOpenDialog: function (instance) {
                        // animate path
                        path.stop().animate({'path': steps.open}, 400, mina.easeinout);
                    },
                    onCloseDialog: function (instance) {
                        // animate path
                        path.stop().animate({'path': steps.close}, 400, mina.easeinout);
                    }
                });
        dlgtrigger.addEventListener('click', dlg.toggle.bind(dlg));
    })();

    function cek(x) {
        if (x > 0) {
            $('#btnSubmit').attr('disabled', false);
        } else {
            $('#btnSubmit').attr('disabled', true);
        }
    }

    function passwordStrengthCheck(oldpassword, password1, password2, passwordsInfo) {

        $(oldpassword).on('keyup', function (e) {
            var pass = oldpassword.val();
            $.post('<?php echo base_url(); ?>admin/oldPassword', {oldpass: pass}, function (c) {
                if (pass === "" || pass === null) {
                    passwordsInfo.removeClass();
                    $('#password1').attr('disabled', true);
                    $('#password2').attr('disabled', true);
                    cek(0);
                } else if (c > 0) {
                    passwordsInfo.removeClass().addClass('goodpass').html("True");
                    $('#password1').attr('disabled', false);
                    cek(0);
                } else {
                    passwordsInfo.removeClass().addClass('weakpass').html("False!");
                    $('#password1').attr('disabled', true);
                    $('#password2').attr('disabled', true);
                    cek(0);
                }

            });
        });

        var WeakPass = /(?=.{8,}).*/;
        var NormalPass = /^(?=\S*?[a-z])\S{8,}$/;
        //Must contain lower case letters and at least one digit.
        var MediumPass = /^(?=\S*?[a-z])(?=\S*?[0-9])\S{8,}$/;
        //Must contain at least one upper case letter, one lower case letter and one digit.
        var StrongPass = /^(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])\S{8,}$/;
        //Must contain at least one upper case letter, one lower case letter and one digit.
        var VryStrongPass = /^(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])(?=\S*?[^\w\*])\S{8,}$/;

        $(password1).on('keyup', function (e) {
            if (password1.val() === "" || password1.val() === null) {
                passwordsInfo.removeClass();
                $('#password2').attr('disabled', true);
                cek(0);
            } else if (VryStrongPass.test(password1.val()))
            {
                passwordsInfo.removeClass().addClass('vrystrongpass').html("Very Strong! (Awesome, please don't forget your pass now!)");
                $('#password2').attr('disabled', false);
                cek(0);
            } else if (StrongPass.test(password1.val()))
            {
                passwordsInfo.removeClass().addClass('strongpass').html("Strong! (Enter special chars to make even stronger");
                $('#password2').attr('disabled', false);
                cek(0);
            } else if (MediumPass.test(password1.val()))
            {
                passwordsInfo.removeClass().addClass('goodpass').html("Good! (Enter uppercase letter to make strong)");
                $('#password2').attr('disabled', false);
                cek(0);
            } else if (NormalPass.test(password1.val()))
            {
                passwordsInfo.removeClass().addClass('stillweakpass').html("Still Weak! (Enter digits to make good password)");
                $('#password2').attr('disabled', true);
                cek(0);
            } else if (WeakPass.test(password1.val()))
            {
                passwordsInfo.removeClass().addClass('stillweakpass').html("Password is Weak!");
                $('#password2').attr('disabled', true);
                cek(0);
            } else
            {
                passwordsInfo.removeClass().addClass('weakpass').html("Very Weak! (Must be 8 or more chars)");
                $('#password2').attr('disabled', true);
                cek(0);
            }
        });

        $(password2).on('keyup', function (e) {

            if (password2.val() === "" || password2.val() === null) {
                passwordsInfo.removeClass();
                cek(0);
            } else if (password1.val() !== password2.val())
            {
                passwordsInfo.removeClass().addClass('weakpass').html("Passwords do not match!");
                cek(0);
            } else {
                passwordsInfo.removeClass().addClass('goodpass').html("Passwords match!");
                cek(1);
            }

        });

    }
    function changePass() {
        $('#btnSubmit').attr('disabled', true);
        var old = $('#oldpassword').val();
        var pass1 = $('#password1').val();
        var pass2 = $('#password2').val();
        if (old === "" || pass1 === "" || pass2 === "" || old === null || pass1 === null || pass2 === null) {
            setAudio('<?php echo base_url(); ?>assets/audio/false.wav');
            swal('Mohon Lengkapi Data');
            $('#btnSubmit').attr('disabled', false);
            return false;
        } else {
            $.post('<?php echo base_url(); ?>Admin/changePassword', {oldpass: old, pass1: pass1, pass2: pass2}, function (d) {
                if (d === "Password Changed") {
                    setAudio('<?php echo base_url(); ?>assets/audio/true.wav');
                    swal(d);
                    $('#password2').attr('disabled', true);
                    $('#password1').attr('disabled', true);
                    $('#btnSubmit').attr('disabled', true);
                    $('#password2').val("");
                    $('#password1').val("");
                    $('#oldpassword').val("");
                    $('#pass-info').removeClass();
                    $('#somedialog').hide();
                    return true;
                } else {
                    setAudio('<?php echo base_url(); ?>assets/audio/false.wav');
                    swal(d);
                    $('#btnSubmit').attr('disabled', false);
                    return false;
                }
            });
        }
    }
</script>