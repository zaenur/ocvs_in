<!DOCTYPE html>   
<html>   
    <head>   
        <meta charset="UTF-8">   
        <title>   
            <?= $title; ?>  
        </title>   
    </head>   
    <body>   
        <h2>Lupa Password</h2>   
        <p>Untuk melakukan reset password, silakan masukkan alamat email anda. </p>   
        <?php echo form_open('Admin/lupaPassword'); ?>   
        <p>Email:</p>   
        <p>   
            <input type="text" name="email" value="<?php echo set_value('email'); ?>"/>   
        </p>   
        <p> <?php echo form_error('email'); ?> </p> 
        <?php echo "<p class='text-danger text-center'>" . $this->session->flashdata('sukses') . "</p>"; ?>
        <p>   
            <input type="submit" name="btnSubmit" value="Submit" />   
        </p>   
    </body>   
</html>