<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $title ?></title>
<link href="<?php echo base_url(); ?>assets/img/bakaranproject.png" rel="shortcut icon" />

<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta name="keywords" content="One Click Voting System" />
<meta name="author" content="Willy" />
<?php $this->load->view('assets/css.php') ?>
<script src="<?php echo base_url(); ?>assets/js/snap.svg-min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>
<script src="<?php echo base_url('assets/js/jquery-2.1.4.min.js') ?>"></script>

<audio style="overflow: hidden;visibility: hidden;" id="beep-one" controls="controls" preload="auto">
    <source src="<?php echo base_url(); ?>assets/audio/false.wav"></source>
    Your browser isn't invited for super fun time.
</audio>