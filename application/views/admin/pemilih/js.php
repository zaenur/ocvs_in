
<?php $this->load->view('assets/js') ?>

<script type="text/javascript">
    $(document).ready(function () {
        $("#tanggal").keypress(function (event) {
            event.preventDefault();
        });
    });
</script>
<script type="text/javascript">
    var table;
    var d = new Date();
    var export_filename = 'OVCS_PASLON_' + d.getTime();
    table = $('#table').DataTable({
        "processing": true,
        "serverSide": true,
        "lengthChange": true,
        dom: 'Bfrtip',
        scrollY: "500px",
        deferRender: true,
        scroller: {
            loadingIndicator: true
        },
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "language": {
            "processing": "Proses bro ....."
        },
        "order": [],
        "lengthMenu": [
            [5, 10, 25, 50, 100],
            ['5 rows', '10 rows', '25 rows', '50 rows', '100 rows']
        ],
        "ajax": {
            "url": "<?php echo site_url('Pemilih/dtServerSide') ?>",
            "type": "POST",
            "data": function (data) {
                data.idpemilih = $('#idpemilih').val();
                data.email = $('#email').val();
                data.tgldaftar = $('#tgldaftar').val();
                data.status = $('#status').val();
            },
            error: function (jqXHR, textStatus, errorThrown) {

                $("#table").append('<h2 class="label-danger text-white">Ngapurane, ana kesalahan nang server ' + jqXHR + textStatus + errorThrown + '</h2>');
                $("#table_processing").css("display", "none");

            }
        },

        buttons: [
            {
                text: '<i class="glyphicon glyphicon-expand"></i> Show 5 rows',
                extend: 'pageLength'
            },
            {
                text: '<i class="glyphicon glyphicon-copy"></i>',
                extend: 'copyHtml5',
                title: export_filename
            },
            {
                text: '<i class="fa fa-lg fa-file-text-o"></i>',
                extend: 'csv',
                title: export_filename,
                extension: '.csv'
            },
            {
                text: '<i class="fa fa-lg fa-file-excel-o"></i>',
                extend: 'excelHtml5',
                title: export_filename,
                extension: '.xlsx'
            }, {
                text: '<i class="fa fa-lg fa-file-pdf-o"></i>',
                extend: 'pdfHtml5',
                title: export_filename,
                extension: '.pdf'
            }, {
                text: '<i class="glyphicon glyphicon-print"></i>',
                extend: 'print',
                title: export_filename
            }, {
                text: '<i class="glyphicon glyphicon-eye-open"></i>',
                extend: 'colvis',
                buttonText: '<img src=&quot;images/down.gif" >',
                activate: 'mouseover',
                exclude: [0, 1],
                columns: ':not(.noVis)'
            }
        ],
        "columnDefs": [
            {
                "targets": [0, 1, 6],
                "orderable": false
            }, {
                targets: [0, 1],
                className: 'noVis'
            }
        ]
    });

    $('.dt-search-input').on('keyup change', function () {
        table.ajax.reload();
    });

    $('#reload').click(function () {
        table.ajax.reload();
    });
    $('#reset').click(function () {
        $('#table').DataTable().search("").draw();
        $('#idpemilih').val("");
        $('#email').val("");
        $('#tgldaftar').val("");
        $('#status').val("");
        table.ajax.reload();
    });

    $("#print_button").click(function () {
        var mode = 'iframe'; // popup
        var close = mode === "popup";
        var options = {mode: mode, popClose: close};
        $("table").printArea(options);
    });

    $('.datepicker').datepicker({
        autoclose: true,
        format: "dd-MM-yyyy",
        todayHighlight: true,
        orientation: "bottom",
        todayBtn: true
    });
    $("input").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    $("#check-all").click(function () {
        $(".data-check").prop('checked', $(this).prop('checked'));
    });

    var save_method;
    var base_url = '<?php echo base_url(); ?>assets/upload/';
    function reload() {
        $('#table').DataTable().search("").draw();
        table.ajax.reload();
    }


    function selectElementContents(el) {
        var body = document.body, range, sel;
        if (document.createRange && window.getSelection) {
            range = document.createRange();
            sel = window.getSelection();
            sel.removeAllRanges();
            try {
                range.selectNodeContents(el);
                sel.addRange(range);
                document.execCommand("Copy");
                sel.removeAllRanges();
                swal({
                    title: "Copied to clipboard",
                    type: "info",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                });
            } catch (e) {
                range.selectNode(el);
                sel.addRange(range);
            }
        } else if (body.createTextRange) {
            range = body.createTextRange();
            range.moveToElementText(el);
            range.select();
            document.execCommand("Copy");
        }
    }


    function add_data() {
        save_method = 'add';
        $('#id').attr('readonly', false);
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show');
        $('.modal-title').text('Tambah Pemilih');
        $('#stpemilih').hide();
    }

    function edit_data(id) {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#stpemilih').show();
        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('Pemilih/editPemilih') ?>",
            type: "POST",
            data: {id: id},
            dataType: "JSON",
            success: function (data)
            {
                $('#id').attr('readonly', true);
                $('[name="id"]').val(data.idpemilih);
                $('[name="emailpemilih"]').val(data.email);
                $('[name="statuspemilih"]').val(data.status);
                $('[name="tgldaftarpemilih"]').datepicker('update', data.tglDaftar);
                $('#modal_form').modal('show');
                $('.modal-title').text('Edit Pemilih');
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax' + jqXHR.getAllResponseHeaders() + textStatus + errorThrown);
            }
        });
    }


    function save() {
        $('#btnSave').html('<i class="glyphicon glyphicon-save"></i> Saving...');
        $('#btnSave').attr('disabled', true); //set button disable
        var url;
        if (save_method == 'add') {
            var id = $('#id').val();
            $.post('<?php echo base_url(); ?>Pemilih/cekId', {id: id}, function (z) {
                if (z == 1)
                {
                    swal({
                        title: "Peringatan !!!",
                        text: "ID Sudah Terdaftar",
                        type: "error",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    });
                    $('#btnSave').html('<i class="glyphicon glyphicon-save"></i> Save');
                    $('#btnSave').attr('disabled', false); //set button enable
                } else {
                    url = "<?php echo site_url('pemilih/addPemilih') ?>";
                    simpan(url);
                }
            });
        } else {
            url = "<?php echo site_url('Pemilih/updatePemilih') ?>";
            simpan(url);
        }
    }

    function simpan(url) {
        // ajax adding data to database
        var formData = new FormData($('#form')[0]);
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data)
            {

                if (data.status)
                {
                    $('#modal_form').modal('hide');
                    reload();
                } else
                {
                    for (var i = 0; i < data.inputerror.length; i++)
                    {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                        $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
                    }
                }
                $('#btnSave').html('<i class="glyphicon glyphicon-save"></i> Save');
                $('#btnSave').attr('disabled', false);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data' + jqXHR.getAllResponseHeaders() + textStatus + errorThrown);
                $('#btnSave').html('<i class="glyphicon glyphicon-save"></i> Save');
                $('#btnSave').attr('disabled', false);
            }

        });
    }

    function hapus_data(id, data) {

        swal({
            title: "Hapus Data !!!",
            text: "Yakin akan menghapus data " + data + "?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            closeOnConfirm: true
        },
                function () {
                    $.ajax({
                        url: "<?php echo site_url('Pemilih/deletePemilih') ?>",
                        type: "POST",
                        data: {id: id},
                        dataType: "JSON",
                        success: function (data)
                        {
                            reload();
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error deleting data' + jqXHR.getAllResponseHeaders() + textStatus + errorThrown);
                        }
                    });
                });
    }

    function bulk_delete() {
        var list_id = [];
        $(".data-check:checked").each(function () {
            list_id.push(this.value);
        });
        if (list_id.length > 0)
        {

            swal({
                title: "Hapus Data !!!",
                text: "Yakin akan menghapus " + list_id.length + " data ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Hapus",
                closeOnConfirm: true
            },
                    function () {
                        $.ajax({
                            type: "POST",
                            data: {id: list_id},
                            url: "<?php echo site_url('Pemilih/bulkDeletePemilih') ?>",
                            dataType: "JSON",
                            success: function (data)
                            {
                                if (data.status)
                                {
                                    reload();
                                    $("#check-all").prop("checked", false);
                                } else
                                {
                                    alert('Failed.');
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown)
                            {
                                alert('Error deleting data' + jqXHR.getAllResponseHeaders() + textStatus + errorThrown);
                            }
                        });
                    });
        } else
        {
            swal({
                title: "Peringatan !!!",
                text: "Silahkan Pilih Data",
                type: "error",
                confirmButtonText: "OK",
                closeOnConfirm: true
            });
        }
    }

    function downloadCSV(csv, filename) {
        var csvFile;
        var downloadLink;
        // CSV file
        csvFile = new Blob([csv], {type: "text/csv"});
        // Download link
        downloadLink = document.createElement("a");
        // File name
        downloadLink.download = filename;
        // Create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);
        // Hide download link
        downloadLink.style.display = "none";
        // Add the link to DOM
        document.body.appendChild(downloadLink);
        // Click download link
        downloadLink.click();
    }

    function exportToCSV() {
        var d = new Date();
        var filename = 'OVCS_PASLON_' + d.getTime();
        var csv = [];
        var rows = document.querySelectorAll("table tr");
        var d = new Date();
        var n = d.getTime();
        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll("td, th");
            for (var j = 0; j < cols.length; j++)
                row.push(cols[j].innerText);
            csv.push(row.join(","));
        }
        downloadCSV(csv.join("\n"), filename + "_" + n + ".csv");
    }


    var method;
    function cancel() {
        $('#importForm')[0].reset();
        $('#importForm').slideToggle();

    }
    function importCSV() {
        method = 'csv';
        $('#importForm')[0].reset();
        $('.title').text('IMPORT CSV');
        $('#btnImport').val('Import');
        $('#btnImport').attr('disabled', false);
        $('#importForm').slideToggle();
        $('#inputFile').attr('accept', 'application/x-csv, text/x-csv, text/csv, application/csv, text/plain, .csv');
//         $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', '', '');
    }
    function importEXCEL() {
        method = 'excel';
        $('#importForm')[0].reset();
        $('.title').text('IMPORT EXCEL');
        $('#btnImport').val('Import');
        $('#btnImport').attr('disabled', false);
        $('#importForm').slideToggle();
        $('#inputFile').attr('accept', 'application/vnd.ms-excel, application/vnd.msexcel, application/excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

    }
    function importP() {
        $('#btnImport').val('Uploading...');
        $('#btnImport').attr('disabled', true);
        var url;
        var filePath = $('#inputFile')[0].value;
        var extn = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
        if (filePath === "" || filePath === null) {
            swal("Pilih File");
            $('#importForm')[0].reset();
            $('#btnImport').val('Import');
            $('#btnImport').attr('disabled', false);

        } else if (!(extn === "csv" || extn === "xls" || extn === "xlsx")) {
            swal("Pilih File Sesuai Format");
            $('#importForm')[0].reset();
            $('#btnImport').val('Import');
            $('#btnImport').attr('disabled', false);

        } else {
            var formData = new FormData($('#importForm')[0]);
            if (method === 'csv') {
                url = "<?php echo site_url('Pemilih/importFromCSV') ?>";
                $.ajax({
                    url: url,
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "JSON",
                    success: function (data)
                    {
                        if (data.status)
                        {
                            swal("Berhasil");
                        } else
                        {
                            swal("Gagal");
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(jqXHR.getAllResponseHeaders() + textStatus + errorThrown);

                    }

                });

            } else {
                url = "<?php echo site_url('Pemilih/importFromExcel') ?>";
                $.ajax({
                    url: url,
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "JSON",
                    success: function (data)
                    {
                        if (data.status)
                        {
                            swal("Berhasil");
                        } else
                        {
                            swal("Gagal" + data.error_string);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error adding / update data' + jqXHR.getAllResponseHeaders() + textStatus + errorThrown);
                    }

                });
            }
            $('#importForm')[0].reset();
            $('#btnImport').val('Import');
            $('#btnImport').attr('disabled', false);
            $('#importForm').slideToggle();
            reload();
        }
    }

    function sendPassword(id, email) {
        $.post('<?php echo base_url(); ?>Pemilih/sendPassword', {id: id}, function (z) {
            // console.log(z);
            if (z == 1)
            {
                // console.log(z);
                swal("Berhasil Mengirim Password Ke " + email);
            } else {
                console.log(z);
                swal("Terjadi Kesalahan");
            }
        });

    }

</script>
