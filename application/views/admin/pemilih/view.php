<!DOCTYPE html>
<html>
    <head> 
        <?php $this->load->view('admin/head.php') ?>  
    </head> 
    <body>
        <div class="ip-container" id="ip-container" style="background-color: transparent; margin-top: -30px;">
            <?php $this->load->view('admin/menu/view'); ?>
            <div class="content-wrap">

                <div class="willy">
                    <center><h3>Daftar Pemilih</h3></center><br/>
                    <div class="dropdown show">

                        <div class="row col-lg-12" style="float: right;">
                            <div class="dropdown show">
                                <button class="btn btn-warning dropdown-toggle" style="float: left;" type="button" id="dropdownMenuButtonExport" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="glyphicon glyphicon-export"></i> Export
                                </button>
                                <div class="dropdown-menu dm" aria-labelledby="dropdownMenuButtonExport">
                                    <a class="dropdown-item dm btn btn-warning" href="<?= base_url() ?>pemilih/exportCSV"><i class="glyphicon glyphicon-file"></i> CSV 1</a>
                                    <a class="dropdown-item dm btn btn-warning" href="javascript:void(0);" onclick="exportToCSV();"><i class="glyphicon glyphicon-file"></i> CSV 2</a>
                                    <a class="dropdown-item dm btn btn-warning" href="<?= base_url() ?>pemilih/downloadEXCEL"><i class="fa fa-file-excel-o"></i> EXCEL</a>
                                    <a class="dropdown-item dm btn btn-warning" href="<?= base_url() ?>pemilih/exportToPDF"><i class="fa fa-file-pdf-o"></i> PDF</a>
                                    <a class="dropdown-item dm btn btn-warning" href="javascript:void(0);" onclick="selectElementContents(document.getElementById('table'));"><i class="glyphicon glyphicon-copy"></i> COPY</a>
                                    <a class="dropdown-item dm btn btn-warning" href="<?= base_url() ?>pemilih/printData" target="_blank"><i class="glyphicon glyphicon-print"></i> PRINT</a>
                                </div>
                            </div>
                            <div class="dropdown show">
                                <button class="btn btn-primary dropdown-toggle" style="float: left;" type="button" id="dropdownMenuButtonImport" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="glyphicon glyphicon-import"></i> Import
                                </button>
                                <div class="dropdown-menu dm" aria-labelledby="dropdownMenuButtonImport">
                                    <a class="dropdown-item dm btn btn-primary" href="javascript:void(0);" onclick="importCSV();"><i class="glyphicon glyphicon-import glyphicon-file"></i> CSV</a>
                                    <a class="dropdown-item dm btn btn-primary" href="javascript:void(0);" onclick="importEXCEL();"><i class="fa fa-file-excel-o"></i> EXCEL</a>
                                    <a class="dropdown-item dm btn btn-primary" href="<?= base_url("assets/format/pemilih/format.xlsx"); ?>"><i class="fa fa-download"></i> Download Format</a>
                                </div>
                            </div>
                            <button class="btn btn-round btn-activated btn-success left" onclick="add_data()" style="float: left;"><i class="glyphicon glyphicon-plus-sign"></i><span> Tambah Pemilih</span></button>
                            <button class="btn btn-round btn-danger" onclick="bulk_delete()" style="float: left;"><i class="glyphicon glyphicon-trash"></i><span> Hapus Data</span></button>
                            <button class="btn btn-round btn-activated btn-round btn-outline-danger left" id="reload" style="float: left;"><i class="glyphicon glyphicon-refresh"></i><span> Reload</span></button>
                            <button class="btn btn-round btn-activated btn-round btn-outline-danger left" id="reset" style="float: right;"><i class="glyphicon glyphicon-remove-circle"></i><span> Reset Search</span></button>
                        </div>
                        <div class="row col-lg-12" id="iform">
                            <form action="javascript:void(0)" method="post" enctype="multipart/form-data" id="importForm">
                                <h4 class="title text-danger"></h4>

                                <input type="file" class="btn btn-sm btn-danger" id="inputFile" name="file"/>
                                <input type="submit" class="btn btn-sm btn-info" name="importSubmit" onclick="importP();" id="btnImport" value="Import">
                                <input type="reset" class="btn btn-sm btn-danger" name="importReset" id="btnImportReset" onclick="cancel();" value="Cancel">
                            </form>
                        </div>
<!--                        <form action="<?= base_url() ?>pemilih/sendPassword" method="post">
                            <h4 class="title text-danger"></h4>
                            <input type="text" class="btn btn-sm btn-danger" id="inputFile" name="id"/>
                            <input type="submit" class="btn btn-sm btn-info" name="importSubmit"  value="Send">
                        </form>-->
                        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="check-all"></th>
                                    <th>No</th>
                                    <th>id Pemilih</th>
                                    <th>Email</th>
                                    <th>Tanggal Daftar</th>
                                    <th>Status</th>
                                    <th style="min-width: 150px;">Action</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <td colspan="2" style="background-color: slategray;"></td>
                                    <td><input type="text" id="idpemilih" name="idpemilih" class="dt-search-input"></td>
                                    <td><input type="text" id="email" name="email" class="dt-search-input"></td>
                                    <td valign="middle"><input type="text" id="tgldaftar" class="dt-search-input datepicker" ></td>
                                    <td>
                                        <select name="status" class="dt-search-input" id="status" required>
                                            <option value=""></option>
                                            <option value="bm">Belum Memilih</option>
                                            <option value="tm">Telah Memilih</option>
                                        </select>
                                    </td>
                                    <td style="background-color: slategray;"></td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div><!-- /content-wrap -->
            </div><!-- /container -->
        </div>
        <footer class="footer">
            <?php $this->load->view('footer'); ?>
        </footer>
        <div class="modal col-lg-12 fade" id="modal_form" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content" style="background-color: #ddd">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <center><h3 class="modal-title">Formulir Pemilih</h3></center>
                    </div>
                    <div class="modal-body form">
                        <form action="#" id="form" class="form-horizontal" autocomplete="off">
                            <!--<input type="hidden" value="" name="id"/>--> 
                            <div class="form-body">
                                <div class="form-group" id="nomor">                                       
                                    <label class="control-label col-md-3">ID Pemilih</label>
                                    <div class="col-md-9 form-control-danger">
                                        <input name="id" class="form-control input" type="text" id="id" required placeholder="ID Pemilih"/>   
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">                                       
                                    <label class="control-label col-md-3">Email</label>
                                    <div class="col-md-9 form-control-danger">
                                        <input name="emailpemilih" class="form-control input" type="email" id="emailpemilih" required placeholder="Email"/>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="stpemilih">                                       
                                    <label class="control-label col-md-3">Status</label>
                                    <div class="col-md-9 form-control-danger">
                                        <select name="statuspemilih" class="form-control input" id="statuspemilih" required>
                                            <option value="">Status</option>
                                            <option value="bm">Belum Memilih</option>
                                            <option value="tm">Telah Memilih</option>
                                        </select>
                                        <!--<input name="statuspemilih" class="form-control input" type="text" id="statuspemilih" required placeholder="status"/>-->
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">                                       
                                    <label class="control-label col-md-3">Tanggal Daftar</label>
                                    <div class="col-md-9 form-control-danger">
                                        <input name="tgldaftarpemilih" class="form-control input datepicker" required="required" readonly type="text" id="tgldaftarpemilih" maxlength="10" placeholder="yyyy-mm-dd"/>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btnSave" onclick="save()" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="glyphicon glyphicon-minus-sign"></i> Cancel</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <?php $this->load->view('admin/pemilih/js') ?>
    </body>
</html>