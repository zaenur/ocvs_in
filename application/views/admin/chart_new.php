<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('admin/head.php') ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bar.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css" integrity="sha256-IvM9nJf/b5l2RoebiFno92E5ONttVyaEEsdemDC6iQA=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css" integrity="sha256-aa0xaJgmK/X74WM224KMQeNQC2xYKwlAt08oZqjeF0E=" crossorigin="anonymous" />
    <style>
        .article {
            background-color: transparent;
            margin-bottom: 50px;
        }

        p {
            color: #ffffff;
            size: 24px;
            font-weight: bold;
            margin-left: 20px;
        }

        table {
            margin-top: 80px;
        }

        .chart {
            margin: auto;
            left: 50%;
        }

        .left {
            text-align: left;
        }
    </style>
</head>

<body>
    <div class="ip-container" id="ip-container">
        <?php $this->load->view('admin/menu/view'); ?>
        <div class="content-wrap">
            <div class="bar-chart">
                <div class="chart clearfix" style="margin-top:100px">
                    <canvas id="myChart" width="400" height="300" style=" margin-bottom: 50px;"></canvas>
                </div>
                <div class="col-lg-10" style="margin: auto; margin-left: 65%; margin-bottom: 100px">
                    <div class="col-lg-10">
                        <a class="btn btn-round btn-block btn-info" href="<?= base_url() ?>laporan/cetaklaporan" target="_blank">
                            <i class="glyphicon glyphicon-print"></i><span> Cetak Laporan</span>
                        </a>
                    </div>
                    <div class="col-lg-5">
                        <!-- <a class="btn btn-round btn-block" href="<?= base_url() ?>laporan/cetaklaporan" target="_blank"> -->
                        <!-- <i class="glyphicon glyphicon-print"></i><span> Cetak Grafik</span> -->
                        <!-- </a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <footer class="footer"> -->
        <?php //$this->load->view('footer'); ?>
    <!-- </footer> -->
</body>

</html>
<?php $this->load->view('assets/js') ?>
<?php
$labels = array();
$prosentase = array();
foreach ($paslon as $p) {
    $labels[] = ucfirst($p->nmketua);
    $suara = $this->Laporan->sum('jmlsuara', 'kdpaslon', $p->kdpaslon)->row();
    if (doubleval($suara->total) != 0) {
        $prosentase[] = (sprintf("%2.2f", doubleval($suara->total) / $totalterdaftar) * 100);
    } else {
        $prosentase[] = 0;
    }
}
?>
<script>
    // For a pie chart
    var suara = <?php echo json_encode($prosentase, TRUE) ?>;
    var nmketua = <?php echo json_encode($labels, TRUE) ?>;
    console.log(suara);
    var ctx = document.getElementById('myChart');
    var myPieChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: nmketua,
            datasets: [{
                data: suara,
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                ],
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
        },
        options: {
            legend: {
                labels: {
                    // This more specific font property overrides the global property
                    fontColor: 'white'
                }
            },
        }
    });
</script>