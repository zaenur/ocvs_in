<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('admin/head.php') ?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bar.css" >
        <style>
            .article{
                background-color: transparent;
                margin-bottom: 50px;
            }
            p{
                color: #ffffff;
                size: 24px;
                font-weight: bold;
                margin-left: 20px;
            }
            table{
                margin-top: 80px;
            }

            .chart{
                margin: auto;
                left: 50%;
            }
            .left{
                text-align: left;
            }


        </style>
    </head>
    <body>
        <div class="ip-container" id="ip-container">
            <?php $this->load->view('admin/menu/view'); ?>
            <div class="content-wrap">
                <div class="bar-chart">

                    <!-- bars -->
                    <div class="chart clearfix" style="margin-top:100px">
                        <?php
                        $total = doubleval($totalsuara->total);

                        foreach ($paslon as $p) {
                            $suara = $this->Laporan->sum('jmlsuara', 'kdpaslon', $p->kdpaslon)->row();
                            if (doubleval($suara->total) != 0) {
                                $prosentase = (sprintf("%2.2f", doubleval($suara->total) / $totalterdaftar) * 100);
                            } else {
                                $prosentase = 0;
                            }
                            ?>

                            <p class="left"><?= ucfirst($p->nmketua) ?></p>
                            <div class="item">
                                <div class="bar">
                                    <span class="percent"><?= $prosentase ?> %</span>
                                    <div class="progress" data-percent="<?= $prosentase ?>">
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="col-lg-10" style="margin: auto; margin-left: 65%; ">
                        <div class="col-lg-5">
                            <a class="btn btn-round btn-block" href="<?= base_url() ?>laporan/cetaklaporan" target="_blank">
                                <i class="glyphicon glyphicon-print"></i><span>  Cetak Laporan</span>
                            </a>
                        </div>
                        <div class="col-lg-5">
                            <a class="btn btn-round btn-block" href="<?= base_url() ?>laporan/cetaklaporan" target="_blank">
                                <i class="glyphicon glyphicon-print"></i><span>  Cetak Grafik</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <?php $this->load->view('footer'); ?>
        </footer>
    </body>
</html>
<?php $this->load->view('assets/js') ?>
<script type="text/javascript" charset="utf-8">

    $(document).ready(function () {
        barChart();

        $(window).resize(function () {
            barChart();
        });

        function barChart() {
            $('.bar-chart').find('.progress').each(function () {
                var itemProgress = $(this),
                        itemProgressWidth = $(this).parent().width() * ($(this).data('percent') / 100);
                itemProgress.css('width', itemProgressWidth);
            });
        }

    });
    function cetak() {
        window.location('laporan/cetaklaporan');
    }

</script>
