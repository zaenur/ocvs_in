<!DOCTYPE html>
<html>
    <head> 
        <?php $this->load->view('admin/head.php') ?>  
    </head> 
    <style>
        button{margin-top: 5px;}
    </style>
    <body>
        <div class="ip-container" id="ip-container" style="background-color: transparent; margin-top: -30px;">
            <?php $this->load->view('admin/menu/view'); ?>

            <div class="willy content-wrap">
                <center><h3>Daftar Pasangan Calon</h3></center><br/>
                <div class="dropdown show">

                    <div class="row col-lg-12" style="float: right;">
                        <div class="dropdown show">
                            <button class="btn btn-warning dropdown-toggle" style="float: left;" type="button" id="dropdownMenuButtonExport" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="glyphicon glyphicon-export"></i> Export
                            </button>
                            <div class="dropdown-menu dm" aria-labelledby="dropdownMenuButtonExport">
                                <a class="dropdown-item dm btn btn-warning" href="<?= base_url() ?>exportCSV" target="_blank"><i class="glyphicon glyphicon-file"></i> CSV 1</a>
                                <a class="dropdown-item dm btn btn-warning" onclick="exportToCSV();" target="_blank"><i class="glyphicon glyphicon-file"></i> CSV 2</a>
                                <a class="dropdown-item dm btn btn-warning" href="<?= base_url() ?>exportToEXCEL" target="_blank"><i class="fa fa-file-excel-o"></i> EXCEL 1</a>
                                <a class="dropdown-item dm btn btn-warning" href="<?= base_url() ?>downloadEXCEL" target="_blank"><i class="fa fa-file-excel-o"></i> EXCEL 2</a>
                                <a class="dropdown-item dm btn btn-warning" onclick="exportToPDF()" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a>
                                <a class="dropdown-item dm btn btn-warning" onclick="selectElementContents(document.getElementById('table'));"><i class="glyphicon glyphicon-copy"></i> COPY</a>
                                <a class="dropdown-item dm btn btn-warning" href="<?= base_url() ?>printData" target="_blank"><i class="glyphicon glyphicon-print"></i> PRINT</a>
                            </div>
                        </div>
                        <div class="dropdown show">
                            <button class="btn btn-primary dropdown-toggle" style="float: left;" type="button" id="dropdownMenuButtonImport" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="glyphicon glyphicon-import"></i> Import
                            </button>
                            <div class="dropdown-menu dm" aria-labelledby="dropdownMenuButtonImport">
                                <a class="dropdown-item dm btn btn-primary" href="javascript:void(0);" onclick="importCSV();"><i class="glyphicon glyphicon-import glyphicon-file"></i> CSV</a>
                                <a class="dropdown-item dm btn btn-primary" href="javascript:void(0);" onclick="importEXCEL();"><i class="fa fa-file-excel-o"></i> EXCEL</a>
                                <a class="dropdown-item dm btn btn-primary" href="<?= base_url("assets/format/paslon/format.csv"); ?>"><i class="fa fa-download"></i> Download Format</a>
                            </div>
                        </div>

                        <button class="btn btn-round btn-activated btn-success left" onclick="add_data()" style="float: left;"><i class="glyphicon glyphicon-plus-sign"></i><span> Tambah Calon</span></button>
                        <button class="btn btn-round btn-danger" onclick="bulk_delete()" style="float: left;"><i class="glyphicon glyphicon-trash"></i><span> Hapus Data</span></button>
                        <button class="btn btn-round btn-activated btn-round btn-outline-danger left" id="reload" onclick="show_data()" style="float: left;"><i class="glyphicon glyphicon-refresh"></i><span> Reload</span></button>
                        <button class="btn btn-round btn-activated btn-round btn-outline-danger left" id="reset" onclick="show_data()" style="float: right;"><i class="glyphicon glyphicon-remove-circle"></i><span> Reset Search</span></button>
                    </div>
                    <div class="row col-lg-12" id="iform">
                        <form action="javascript:void(0)" method="post" enctype="multipart/form-data" id="importForm">
                            <h4 class="title text-danger"></h4>

                            <input type="file" class="btn btn-sm btn-danger" id="inputFile" name="file"/>
                            <input type="submit" class="btn btn-sm btn-info" name="importSubmit" onclick="importP();" id="btnImport" value="Import">
                            <input type="reset" class="btn btn-sm btn-danger" name="importReset" id="btnImportReset" onclick="cancel();" value="Cancel">
                        </form>
                    </div>

               <!--                            <table id="example1" width="100%" class="table table-bordered" >
                   <thead>
                       <tr>
                           <th><input type="checkbox" id="check-all"></th>
                           <th>Kode</th>
                           <th>Nama Ketua</th>
                           <th>Nama Wakil</th>
                           <th>Motto</th>
                           <th>Tanggal Daftar</th>
                           <th>Foto</th>
                           <th style="min-width: 150px;">Action</th>
                       </tr>
                   </thead>
                   <tbody id="show_data">
               
                   </tbody>
               
                   <tfoot>
                       <tr>
                           <th></th>
                           <th>Kode</th>
                           <th>Nama Ketua</th>
                           <th>Nama Wakil</th>
                           <th>Motto</th>
                           <th>Tanggal Daftar</th>
                           <th>Foto</th>
                           <th style="min-width: 150px;">Action</th>
                       </tr>
                   </tfoot>
               </table>-->
                    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="check-all"></th>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Nama Ketua</th>
                                <th>Nama Wakil</th>
                                <th>Motto</th>
                                <th>Tanggal Daftar</th>
                                <th>Foto</th>
                                <th style="min-width: 150px;">Action</th>
                            </tr>
                        </thead>
                        <thead style="background-color: #ffffff;">
                            <tr>
                                <td colspan="2" style="background-color: slategray;"></td>
                                <td><input type="text" id="kode" name="kode" class="dt-search-input"></td>
                                <td><input type="text" id="ketua" name="ketua" class="dt-search-input"></td>
                                <td><input type="text" id="wakil" name="wakil" class="dt-search-input"></td>
                                <td><input type="text" id="moto" name="moto" class="dt-search-input"></td>
                                <td valign="middle"><input type="text" id="tgl" class="dt-search-input datepicker" ></td>
                                <td colspan="3" style="background-color: slategray;"></td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <footer class="footer">
            <?php $this->load->view('footer'); ?>
        </footer>
        <div class="modal col-lg-12 fade" id="modal_form" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content" style="background-color: #ddd">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <center><h3 class="modal-title">Formulir Calon</h3></center>
                    </div>
                    <div class="modal-body form">
                        <form action="#" id="form" class="form-horizontal" autocomplete="off">
                            <!--<input type="hidden" value="" name="id"/>--> 
                            <div class="form-body">
                                <div class="form-group" id="nomor">                                       
                                    <label class="control-label col-md-3">Kode / Nomor urut</label>
                                    <div class="col-md-9 form-control-danger">
                                        <input name="id" class="form-control input" type="text" id="id" required placeholder="Kode / Nomor urut"/>                                       
                                    </div>
                                </div>
                                <div class="form-group">                                       
                                    <label class="control-label col-md-3">Nama Calon Ketua</label>
                                    <div class="col-md-9 form-control-danger">
                                        <input name="nmketua" class="form-control input" type="text" id="nmketua" required placeholder="Calon Ketua"/>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">                                       
                                    <label class="control-label col-md-3">Nama Calon Wakil Ketua</label>
                                    <div class="col-md-9 form-control-danger">
                                        <input name="nmwakil" class="form-control input" type="text" id="nmwakil" required placeholder="Calon Wakil Ketua"/>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">                                       
                                    <label class="control-label col-md-3">Tanggal Daftar</label>
                                    <div class="col-md-9 form-control-danger">
                                        <input name="tgldaftar" class="form-control input datepicker" required="required" readonly type="text" id="tanggal" maxlength="10" placeholder="yyyy-mm-dd"/>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Motto</label>
                                    <div class="col-md-9 form-control-danger">
                                        <textarea name="motto" class="form-control input" placeholder="Motto" style="resize: none; height: 100px;"></textarea>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="photo-preview">
                                    <label class="control-label col-md-3">Photo</label>
                                    <div class="col-md-9">
                                        (No photo)
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">                                       
                                    <label class="control-label col-md-3" id="label-photo">Upload Photo</label>
                                    <div class="col-md-9 form-control-danger">
                                        <input name="photo" class=" btn btn-danger" type="file" id="foto" accept="image/*" required placeholder="Choose Photo"/>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btnSave" onclick="save()" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="glyphicon glyphicon-minus-sign"></i> Cancel</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <?php $this->load->view('admin/paslon/js') ?>
    </body>
</html>