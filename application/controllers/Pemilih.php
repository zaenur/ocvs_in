<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin
 *
 * @author Willy
 */
class Pemilih extends Admin_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $login = $this->AdminIsLoggedIn();
        $data = array(
            'title' => 'OVCS Admin',
            'login' => $login
        );

        if ($login === TRUE) {
            $data['title'] = 'OVCS Pemilih';
            $this->load->view('admin/pemilih/view', $data);
        } else {
            $this->load->view('admin/login/view', $data);
        }
    }

    public function dtServerside() {
        $list = $this->Pemilih->getDataTables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $no++;
            $row = array();
            $row[] = "<input type='checkbox' class='data-check' value='" . $value->idpemilih . "'/>";
            $row[] = $no;
            $row[] = $value->idpemilih;
            $row[] = $value->email;
            $row[] = $value->tglDaftar;
            if ($value->status == "tm") {
                $row[] = 'Telah Memilih';
            } else {
                $row[] = 'Belum Memilih';
            }
            $row[] = "<td style='text-align:center; vertical-align: central;'>"
                    . "<a style='margin-right: 10px; margin-bottom: 10px;' class='btn btn-sm btn-primary btn-block btn-info' href='javascript:void(0);'"
                    . "title='kirim' onclick='sendPassword(" . '"' . $value->idpemilih . '"' . "," . '"' . $value->email . '"' . ")'>"
                    . "<i class='glyphicon glyphicon-envelope'></i> Kirim Password</a>"
                    . "<a style='margin-right: 10px;' class='btn btn-sm btn-primary btn-warning' href='javascript:void(0);'"
                    . "title='edit' onclick='edit_data(" . '"' . $value->idpemilih . '"' . ")'>"
                    . "<i class='glyphicon glyphicon-pencil'></i> Edit</a>"
                    . "<a class='btn btn-sm btn-danger' href='javascript:void(0);'"
                    . "title='hapus' onclick='hapus_data(" . '"' . $value->idpemilih . '"' . "," . '"' . $value->email . '"' . ")'>"
                    . "<i class='glyphicon glyphicon-trash'></i> Hapus</a></td>";

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Pemilih->countAll(),
            "recordsFiltered" => $this->Pemilih->countFiltered(),
            "data" => $data
        );
        echo json_encode($output);
    }

    public function editPemilih() {
        $id = $this->input->post('id');
        $data = $this->Pemilih->show('idpemilih', $id)->row();
//        $data->tgldaftar = ($data->tgldaftar == '0000-00-00') ? '' : $data->tgldaftar;
        echo json_encode($data);
    }

    public function updatePemilih() {
        $idpemilih = $this->input->post('id');
        $email = $this->input->post('emailpemilih');
        $status = $this->input->post('statuspemilih');
        $tgldaftar = $this->input->post('tgldaftarpemilih');
        $this->_validasi();

        if ($status == "") {
            $status = "bm";
        }
        $data = array(
            'email' => $email,
            'status' => $status,
            'tglDaftar' => $tgldaftar,
        );
        $this->Pemilih->update($data, array('idpemilih' => $idpemilih));
        echo json_encode(array("status" => TRUE));
    }

    public function cekID() {
        $id = $this->input->post('id');
        $res = $this->Pemilih->selectCount('idpemilih', $id)->result();
        if ($res[0]->cnt > 0) {
            echo '1';
        } else {
            echo '0';
        }
    }

    public function sendPassword() {
        $idpemilih = $this->input->post('id');
        $pemilih = $this->Pemilih->where('idpemilih', $idpemilih)
                        ->get()->row();
        $id = $pemilih->idpemilih;
        $email = $pemilih->email;
        $pass = $this->generatePassword(5);
        $data = array(
            'id' => $id,
            'email' => $email,
            'password' => $pass
        );
        $message = $this->load->view('admin/email', $data, TRUE);
        $this->kirimEmail($email, "Intermedia OCVS Login Account", $message);
        // echo $pass;
        $dSave = [
            // 'passpemilih' => $this->bcrypt->hash_password($pass)
            'passpemilih' => $pass
        ];
        if (!$this->email->send()) {
            echo $this->email->print_debugger();
        } else { 
            if ($this->Pemilih->update($dSave, array('idpemilih' => $idpemilih))) {
                // echo $passss;
                echo '1';
            } else {
                // echo $passss;
                echo '0';
                // echo $this->email->print_debugger();
            }
        }
        //  echo json_encode(array("status" => TRUE));
    }

    public function addPemilih() {

        $idpemilih = $this->input->post('id');
        $email = $this->input->post('emailpemilih');
        $tgldaftar = $this->input->post('tgldaftarpemilih');
        $this->_validasi();

        $password = $this->generatePassword(10);
        $d = array(
            'id' => $idpemilih,
            'email' => $email,
            'password' => $password
        );

        $message = $this->load->view('admin/email', $d, TRUE);
        $this->kirimEmail($email, "Password Login", $message);

        $data = array(
            'idpemilih' => $idpemilih,
            'email' => $email,
            'tglDaftar' => $tgldaftar,
            // 'passpemilih' => $this->bcrypt->hash_password($password)
              'passpemilih' => $password
        );


        $this->Pemilih->simpan($data);
        echo json_encode(array("status" => TRUE));
    }

    private function _validasi() {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
        $idpemilih = $this->input->post('id');
        $email = $this->input->post('emailpemilih');
        $tgldaftar = $this->input->post('tgldaftarpemilih');

        $this->form_validation->set_rules('emailpemilih', 'emailpemilih', 'required|valid_email|trim|htmlspecialchars');
        if ($this->form_validation->run('emailpemilih') == FALSE) {
            $data['inputerror'][] = 'emailpemilih';
            $data['error_string'][] = 'Email Tidak Sesuai';
            $data['status'] = FALSE;
        }

        if ($idpemilih == "") {
            $data['inputerror'][] = 'id';
            $data['error_string'][] = 'ID Pemilih Harus Diisi';
            $data['status'] = FALSE;
        }
        if ($email == "") {
            $data['inputerror'][] = 'emailpemilih';
            $data['error_string'][] = 'Email Harus Diisi';
            $data['status'] = FALSE;
        }
        if ($tgldaftar == "") {
            $data['inputerror'][] = 'tgldaftarpemilih';
            $data['error_string'][] = 'Tanggal daftar Harus Diisi';
            $data['status'] = FALSE;
        }
        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }

    public function deletePemilih() {
        $id = $this->input->post('id');
        $this->Pemilih->hapus('idpemilih', $id);
        echo json_encode(array("status" => TRUE));
    }

    public function bulkDeletePemilih() {
        $daftarHapus = $this->input->post('id');
        foreach ($daftarHapus as $id) {
            $this->Pemilih->hapus('idpemilih', $id);
        }
        echo json_encode(array("status" => TRUE));
    }

    public function importFromCSV() {
        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if (!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)) {
            if (is_uploaded_file($_FILES['file']['tmp_name'])) {
                $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                fgetcsv($csvFile);

                while (($line = fgetcsv($csvFile)) !== FALSE) {
                    $query = $this->Pemilih->custom("SELECT idpemilih FROM pemilih WHERE idpemilih = '" . $line[0] . "'")->result();
                    if (count($query) > 0) {
                        $data = array(
                            'email' => $line[1],
                            'tglDaftar' => $line[2],
                            'status' => $line[3]
                        );
                        $this->Pemilih->update($data, array('idpemilih' => $line[0]));
                    } else {
                        if ($line[0] == "") {
                            $line[0] = 1;
                        }
                        $data = array(
                            'idpemilih' => $line[0],
                            'email' => $line[1],
                            'tglDaftar' => $line[2],
                            'status' => $line[3]
                        );
                        $this->Pemilih->simpan($data);
                    }
                }
                fclose($csvFile);
            } else {
                echo json_encode(array("status" => FALSE));
            }
        } else {
            echo json_encode(array("status" => FALSE));
        }
        echo json_encode(array("status" => TRUE));
    }

    function importFromExcel() {
        $this->load->library('PHPExcel');
        $this->load->library('upload');
        $fileName = time() . $_FILES['file']['name'];
        $config['upload_path'] = 'assets/upload/fileExcel/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = "xls|xlsx|csv";
        $config['max_size'] = 10000;
//

        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file')) {
            $data['inputerror'][] = 'file';
            $data['error_string'][] = 'Upload error: ' . $this->upload->display_errors(' ', ' ');
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        } else {
            $media = $this->upload->data();
            $inputFileName = 'assets/upload/fileExcel/' . $media['file_name'];
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                $data['inputerror'][] = 'file';
                $data['error_string'][] = 'Error loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                $data['status'] = FALSE;
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            for ($row = 2; $row <= $highestRow; $row++) {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $query = $this->Pemilih->custom("SELECT idpemilih FROM pemilih WHERE idpemilih = '" . $rowData[0][0] . "'")->result();
                if (count($query) > 0) {
                    $data = array(
                        "email" => $rowData[0][1],
                        "tglDaftar" => $rowData[0][2],
                        "status" => $rowData[0][3]
                    );
                    $this->Pemilih->update($data, array('idpemilih' => $rowData[0][0]));
                } else {
                    if ($rowData[0][0] == "") {
                        $rowData[0][0] = 1;
                    }
                    $data = array(
                        "idpemilih" => $rowData[0][0],
                        "email" => $rowData[0][1],
                        "tglDaftar" => $rowData[0][2],
                        "status" => $rowData[0][3]
                    );
                    $this->Pemilih->simpan($data);
                }
//                delete_files($media['file_path']);
            }
            echo json_encode(array("status" => TRUE));
        }
    }

    function exportCSV() {
        $data = $this->Pemilih->get();
        $cek = $data->num_rows();
        if ($cek > 0) {
            $query = $data->result_array();
            $delimiter = ",";
            $filename = "pemilih_" . date('d-m-Y') . ".csv";

            $f = fopen('php://memory', 'w');

            $fields = array('ID Pemilih', 'Email', 'Tanggal Daftar', 'Status');
            fputcsv($f, $fields, $delimiter);
            foreach ($query as $row) {
                $lineData = array($row['idpemilih'], $row['email'], $row['tglDaftar'], $row['status']);
                fputcsv($f, $lineData, $delimiter);
            }
            fseek($f, 0);
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            fpassthru($f);
        }
        exit;
    }

    function exportToPDF() {
        $this->load->helper('tanggal');
        $data['title'] = 'Data Pemilih';
        $data['tgl'] = hariTanggalBulanTahun();
        $data['pemilih'] = $this->Pemilih->show('', '')->result();
        $view = "admin/cetak/pemilihPDF";
        $this->load->view($view, $data);
        $this->exportPDFP($view, $data);
    }

    function printData() {
        $data['title'] = 'Data Pemilih';
        $data['pemilih'] = $this->Pemilih->show('', '')->result();
        $data['tgl'] = $this->tgl_indo(date('d-m-Y'));
        $this->load->view('admin/cetak/pemilihCetak', $data);
    }

    function downloadExcel() {

        $this->load->library('PHPExcel');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(17);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);

        $objPHPExcel->getActiveSheet()->getStyle(1)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle(2)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle(3)->getFont()->setBold(true);

        $header = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'FF0000'),
                'name' => 'Courier new'
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle("A1:F2")
                ->applyFromArray($header)
                ->getFont()->setSize(16);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:E2');
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Data Pemilih')
                ->setCellValue('A3', 'No')
                ->setCellValue('B3', 'ID Pemilih')
                ->setCellValue('C3', 'Email')
                ->setCellValue('D3', 'Tanggal Daftar')
                ->setCellValue('E3', 'Status');

        $ex = $objPHPExcel->setActiveSheetIndex(0);
        $no = 1;
        $counter = 4;
        $query = $this->Pemilih->show('', '')->result();
        foreach ($query as $row):

            $ex->setCellValue('A' . $counter, $no++);
            $ex->setCellValue('B' . $counter, $row->idpemilih);
            $ex->setCellValue('C' . $counter, $row->email);
            $ex->setCellValue('D' . $counter, $row->tglDaftar);
            $ex->setCellValue('E' . $counter, $row->status);

            $counter = $counter + 1;
        endforeach;

        $objPHPExcel->getProperties()->setCreator("Yoga Willy")
                ->setLastModifiedBy("Yoga Willy")
                ->setTitle("Export To EXCEL")
                ->setSubject("Export To Excel")
                ->setDescription("Office 2007 XLS, XLSX")
                ->setKeywords("office 2007 openxml php")
                ->setCategory("Excel");
        $objPHPExcel->getActiveSheet()->setTitle('Data Calon');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Last-Modified:' . gmdate("D, d M Y H:i:s") . 'GMT');
        header('Chace-Control: no-store, no-cache, must-revalation');
        header('Chace-Control: post-check=0, pre-check=0', FALSE);
        header('Pragma: no-cache');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="DataPasanganCalon' . date('Ymd') . '.xlsx"');

        $objWriter->save('php://output');
    }

}
