<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Error
 *
 * @author Willy
 */
class Error extends MY_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
    }

    public function index() {
//        $this->load->library('parser');
//        $contoh = "Bakaran Project";
//        $this->parser->parse('err/v_404', $contoh);
        $this->load->view('err/v_404');
    }

}
