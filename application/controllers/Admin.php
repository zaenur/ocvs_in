<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin
 *
 * @author Willy
 */
class Admin extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('Ajax_pagination');
        $this->perPage = 2;
    }

    //Login
    public function index() {
        $login = $this->AdminIsLoggedIn();
        $data = array(
            'title' => 'OVCS Admin',
            'login' => $login
        );
        if ($login === TRUE) {
            $data['username'] = strtoupper($this->session->userdata['admin_data']['username']);
            $this->load->view('admin/dashboard', $data);
        } else {
            $this->load->view('admin/login/view', $data);
        }
    }

    public function cekUser() {
        $username = $this->input->post('username');
        $res = $this->Admin->selectCount('username', $username)->result();
        if ($res[0]->cnt > 0) {
            echo '1';
        } else {
            echo '0';
        }
    }

    public function cekPassword() {
        $username = $this->input->post('username');
        $password = $this->input->post('pass');
        $query = $this->Admin->show('username', $username);
        $cek = $query->num_rows();
        if ($cek > 0) {
            $q = $query->result();
            foreach ($q as $value) {
                if ($this->bcrypt->check_password($password, $value->password) && $value->status == "active") {
                    if (!(empty($value->lastLogin) || is_null($value->lastLogin))) {
                        $last = "Terakhir login pada " . $value->lastLogin;
                    } else {
                        $last = "Selamat, ini adalah pertama kali Anda login";
                    }
                    $data = array('kdadmin' => $value->kdadmin, 'username' => $value->username, 'otoritas' => $value->otoritas);
                    $this->session->set_userdata('admin_data', $data);
                    $this->load->helper('tanggal');
                    $tgl = hariTanggalBulanTahun();
                    $waktu = date('G:i:s');
                    $lastLogin = array(
                        'lastLogin' => $tgl . " " . $waktu
                    );
                    $this->Admin->update($lastLogin, array('kdadmin' => $value->kdadmin));
                    echo $last;
                } else {
                    echo '0';
                }
            }
        }
    }

    function lupaPassword() {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = 'Halaman Reset Password';
            $this->load->view('admin/lupapassword', $data);
        } else {
            $email = $this->input->post('email');
            $clean = $this->security->xss_clean($email);
            $user = $this->Admin->where('username', $clean)
                            ->get()->row();
            if (!$user) {
                $this->session->set_flashdata('sukses', 'email address salah, silakan coba lagi.');
                $data['title'] = 'Halaman Reset Password';
                $this->load->view('admin/lupapassword', $data);
            } else {
                $token = $this->Admin->insertToken($user->kdadmin);
                $qstring = $this->base64url_encode($token);
                $url = site_url() . '/Admin/resetPassword/token/' . $qstring;
                $link = '<a href="' . $url . '">' . $url . '</a>';

                $message = '';
                $message .= '<strong>Hai, anda menerima email ini karena ada permintaan untuk memperbaharui  
                 password anda.</strong><br>';
                $message .= '<strong>Silakan klik link ini:</strong> ' . $link;

                echo $message; //send this through mail  
                exit;
            }
        }
    }

    public function resetPassword() {
        $token = $this->base64url_decode($this->uri->segment(4));
        $cleanToken = $this->security->xss_clean($token);

        $user_info = $this->Admin->isTokenValid($cleanToken); //either false or array();          

        if (!$user_info) {
            $this->session->set_flashdata('sukses', 'Token tidak valid atau kadaluarsa');
            redirect(site_url('Admin'), 'refresh');
        }

        $data = array(
            'title' => 'Halaman Reset Password ',
            'nama' => $user_info->kdadmin,
            'email' => $user_info->username,
            'token' => $this->base64url_encode($token)
        );

        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/resetpassword', $data);
        } else {

            $post = $this->input->post(NULL, TRUE);
            $cleanPost = $this->security->xss_clean($post);
            $hashed = md5($cleanPost['password']);
            $cleanPost['password'] = $hashed;
            $cleanPost['id_user'] = $user_info->kdadmin;
            unset($cleanPost['passconf']);
            if (!$this->m_account->updatePassword($cleanPost)) {
                $this->session->set_flashdata('sukses', 'Update password gagal.');
            } else {
                $this->session->set_flashdata('sukses', 'Password anda sudah  
             diperbaharui. Silakan login.');
            }
            redirect(site_url('login'), 'refresh');
        }
    }

    public function base64url_encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    public function base64url_decode($data) {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    function logOut() {
        $sess_array = array(
            'username' => ''
        );
        $this->session->unset_userdata('admin_data', $sess_array);
        $this->session->sess_destroy();
        redirect('/home', 'refresh');
        exit();
    }

    //Admin Account
    function setting() {
        $login = $this->AdminIsLoggedIn();
        if ($login === TRUE) {
            $data = array();
            $totalRec = count($this->Admin->getRows());

            $config['target'] = '#postList';
            $config['base_url'] = base_url() . 'Admin/paginationData';
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $config['link_func'] = 'searchFilter';
            $this->ajax_pagination->initialize($config);

            //get the posts data
            $data['posts'] = $this->Admin->getRows(array('limit' => $this->perPage));

            $data['login'] = $login;
            $data['title'] = 'OVCS Administrator';
            $this->load->view('admin/akun/view', $data);
        } else {
            $data = array(
                'title' => 'OVCS Admin',
                'login' => $login
            );
            $this->load->view('admin/login/view', $data);
        }
    }

    function paginationData() {
        $conditions = array();
        //calc offset number
        $page = $this->input->post('page');
        if (!$page) {
            $offset = 0;
        } else {
            $offset = $page;
        }

//        //set conditions for search
        $keywords = $this->input->post('keywords');
        $sortBy = $this->input->post('sortBy');
        if (!empty($keywords)) {
            $conditions['search']['keywords'] = $keywords;
        }
        if (!empty($sortBy)) {
            $conditions['search']['sortBy'] = $sortBy;
        }
        //total rows count
        $totalRec = count($this->Admin->getRows($conditions));
//        $totalRec = count($this->Admin->getRows());
        //pagination configuration
        $config['target'] = '#postList';
        $config['base_url'] = base_url() . 'Admin/paginationData';
        $config['total_rows'] = $totalRec;
        $config['per_page'] = $this->perPage;
        $config['link_func'] = 'searchFilter';
        $this->ajax_pagination->initialize($config);

        //set start and limit
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        //get posts data
        $data['posts'] = $this->Admin->getRows($conditions);
//        $data['posts'] = $this->Admin->getRows(array('start' => $offset, 'limit' => $this->perPage));
        //load the view
        $this->load->view('admin/akun/pagination', $data, false);
    }

    function cekUserName() {
        $username = $this->input->post('username');
        $res = $this->Admin->selectCount('username', $username)->result();
        if ($res[0]->cnt > 0) {
            echo '1';
        } else {
            echo '0';
        }
    }

    function tambahAdmin() {

        $username = $this->input->post('username');
        $otoritas = $this->input->post('otoritas');

        // $pwd = $this->generatePassword(10);
        $pwd = 'bakaranproject';
        $password = password_hash($pwd, PASSWORD_DEFAULT);
       // $password = $this->bcrypt->hash_password($pwd);
        $this->_validate($username, $otoritas, $pwd);

        $tbadmin = array(
            'username' => $username,
            'otoritas' => $otoritas,
            'password' => $password,
            'status' => 'active'
        );

        $d = array(
            'email' => $username,
            'id' => $username,
            'password' => $pwd
        );
        $message = $this->load->view('admin/email', $d, TRUE);
        $this->kirimEmail($username, "Password Login", $message);
        $this->Admin->simpan($tbadmin);

        echo json_encode(array("status" => TRUE));
    }

    function changeStatus() {
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        if ($this->Admin->update(array('status' => $status), array('kdadmin' => $id))) {
            echo 1;
        }
    }

    function _validate($username, $otoritas, $password) {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
        $this->form_validation->set_rules('username', 'username', 'required|valid_email|trim|htmlspecialchars');
        if ($this->form_validation->run('username') == FALSE) {
            $data['inputerror'][] = 'username';
            $data['error_string'][] = 'Email tidak sesuai';
            $data['status'] = FALSE;
        }
        if ($username == "") {
            $data['inputerror'][] = 'username';
            $data['error_string'][] = 'Username Harus Diisi';
            $data['status'] = FALSE;
        }
        if ($otoritas == "") {
            $data['inputerror'][] = 'otoritas';
            $data['error_string'][] = 'Otoritas Harus Dipilih';
            $data['status'] = FALSE;
        }
        if ($password == "") {
            $data['status'] = FALSE;
        }
        if ($data['status'] == FALSE) {
            echo json_encode($data);
            exit();
        }
        echo json_encode(array("status" => TRUE));
    }

    public function deleteAdmin() {
        $id = $this->input->post('id');
        $this->Admin->hapus('kdadmin', $id);
        echo json_encode(array("status" => TRUE));
    }

    //Change Admin Password
    public function oldPassword() {
        $username = $this->session->userdata['admin_data']['username'];
        $password = $this->input->post('oldpass');

        $query = $this->Admin->show('username', $username);
        $q = $query->row();
        $pass = $q->password;
        if ($this->bcrypt->check_password($password, $pass)) {
            echo '1';
        } else {
            echo '0';
        }
    }

    public function changePassword() {

        $kode = $this->session->userdata['admin_data']['kdadmin'];
        $oldpass = $this->input->post('oldpass');
        $pass1 = $this->input->post('pass1');
        $pass2 = $this->input->post('pass2');
        $query = $this->Admin->show('kdadmin', $kode);
        $q = $query->row();
        $pass = $q->password;

        if ($this->bcrypt->check_password($oldpass, $pass)) {
            if ($pass1 == "" || $pass2 == "") {
                echo 'Password Empty ';
            } else {
                if ($pass1 == $pass2) {
                    $password = $this->bcrypt->hash_password($pass2);
                    $data = array(
                        'password' => $password,
                    );
                    $this->Admin->update($data, array('kdadmin' => $kode));
                    echo 'Password Changed';
                } else {
                    echo 'Password Do Not Match';
                }
            }
        } else {
            echo 'Password False';
        }
    }

}
