<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('dataModel');
    }

    public function index() {
        $login = $this->dataModel->isLoggedIn();
        if ($login === TRUE) {
            $data["calon"] = $this->dataModel->show("calonimedketua", "", "")->result_array();
            $this->load->view('oc/pilih', $data);
        } else {
            $this->load->view('oc/index');
        }
    }

    public function cekLogin() {
        $nim = $this->input->post('nim');
        $this->dataModel->cekN('daftartb', 'nim', $nim);
    }

    public function cekPakai() {
        $nim = $_POST['nim'];
        $this->dataModel->cekN('pilihtb', 'nim', $nim);
    }

    public function login() {
        $nim = $this->input->post('nim');
        $this->dataModel->login('daftartb', 'nim', $nim);
    }

//    public function proses() {
//        $nim = $this->input->post('nim');
//        $nim = str_replace('_', '', $nim);
//        $count = strlen($nim);
//        if ($count == 2) {
//            echo'<script> alert("Maaf\nMohon Masukan NIM Anda"); </script>';
////            $this->load->view('oc/index');
//            redirect('/');
//        } elseif ($count < 10) {
//            echo'<script> alert("Maaf\nMasukan NIM Dengan Benar");</script>';
//            $this->load->view('oc/index');
//        } else {
//            $cek = $this->dataModel->cekN('daftartb', 'nim', $nim);
//            $login = $this->dataModel->cekN('pilihtb', 'nim', $nim);
//            if ($cek === 0) {
//                echo'<script> alert("Maaf\nNIM Belum Terdaftar\nSilahkan Daftarkan NIM Anda");</script>';
//                $this->load->view('oc/index');
//            } else if ($login === 1) {
//                echo "<script type=\"text/javascript\"> alert(\"Kode Telah Terpakai !\"); </script>";
//                $this->load->view('oc/index');
//            } else {
//                $this->dataModel->login('daftartb', 'nim', $nim);
//                $this->load->view('oc/pilih');
//            }
//        }
//    }
}
