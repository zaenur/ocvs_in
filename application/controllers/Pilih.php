<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pilih
 *
 * @author Willy
 */
class Pilih extends User_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));

    }

    public function index() {
      $login = $this->UserIsLoggedIn();
      $data = array(
          'title' => 'OVCS',
          'login' => $login
      );

        if ($login === TRUE) {
            $data['username'] = strtoupper($this->session->userdata['user_data']['idpemilih']);
            $data["calon"] = $this->Pilih->tampilPaslon("", "")->result_array();
            $this->load->view('oc/pilih', $data);
        } else {
            $this->load->view('oc/index');
        }

    }


    public function cekLogin() {
        $nim = $this->input->post('nim');
        $res = $this->Pilih->cekNIM($nim)->result();
        if ($res[0]->cnt > 0) {
            echo '1';
        } else {
            echo '0';
        }
    }

    public function cekPassword() {

        $nim = $this->input->post('nim');
        $password = $this->input->post('pass');
        $query = $this->Pilih->tampil('idpemilih', $nim);
        $cek = $query->num_rows();
        if ($cek > 0) {
            $q = $query->result();
            foreach ($q as $value) {
                if ($password == $value->passpemilih) {
                    echo '1';
                } else {
                    echo '0';
                }
                 // if ($this->bcrypt->check_password($password, $value->passpemilih)) {
                 //      $data = array('idpemilih' => $value->idpemilih);
                 //      $this->session->set_userdata('user_data', $data);
                 //      echo '1';
                 //  } else {
                 //      echo '0';
                 //  }
            }
        }
    }

    public function cekPakai() {
        $nim = $this->input->post('nim');
        $query = $this->Pilih->tampil('idpemilih', $nim)->result();
        foreach ($query as $value) {
            if ($value->status == "bm" || $value->status == "BM") {
              $data = array('idpemilih' => $value->idpemilih, 'email' => $value->email);
              $this->session->set_userdata('user_data', $data);
                echo '0';
            } else {
                echo '1';
            }

        }
    }

    public function proses() {
        $id = $this->input->get('id');
//        echo '<script> alert( ' . $id . '); </script>';
        if ($id != "") {
            $login = $this->UserIsLoggedIn();
            if ($login === TRUE) {
                $hitung = $this->Pilih->hitung($id)->result();
                // die(json_encode)
                $jumlahsuara = $hitung[0]->jmlsuara + 1;
                $this->Pilih->updateHitung(array('jmlsuara' => $jumlahsuara), array('kdpaslon' => $id));
                $this->Pilih->updatePemilih(array('status' => "tm"), array('idpemilih' => $this->session->userdata['user_data']['idpemilih']));
                $this->load->view('oc/q');
                $this->session->sess_destroy();
//                 echo "<meta http-equiv='refresh' content='3' URL= 'aaa.com'>";
//                sleep(10);
//                redirect(base_url());
//                sleep(5);
//                header("Location: ");
                header("Refresh:5; URL = " . base_url() . "pilih" );
            } else {
                $this->load->view('oc/index');
            }
        } else {
            $data["calon"] = $this->Pilih->show("paslon", "")->result_array();
            $this->load->view('oc/pilih', $data);
        }

        //$this->load->view('oc/pilih');
    }

    public function thanks(){
        $this->load->view('oc/q');
    }

}
