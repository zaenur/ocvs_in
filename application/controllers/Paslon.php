<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin
 *
 * @author Willy
 */
class Paslon extends Admin_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    public function dtServerside() {
        $list = $this->Paslon->getDataTables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $value) {
            $no++;
            $row = array();
            $row[] = "<input type='checkbox' class='data-check' value='" . $value->kdpaslon . "'/>";
            $row[] = $no;
            $row[] = $value->kdpaslon;
            $row[] = $value->nmketua;
            $row[] = $value->nmwakil;
            $row[] = $value->motto;
            $row[] = $value->tgldaftar;
            if ($value->foto) {
                $row[] = '<a href="' . base_url('assets/upload/' . $value->foto) . '"'
                        . ' target="_blank"><img src="' . base_url('assets/upload/' . $value->foto) . '" class="img-responsive" style=" max-height: 50px;" /></a>';
            } else {
                $row[] = '(No photo)';
            }
            $row[] = "<td style='text-align:center; vertical-align: middle;'><a style='margin-right: 10px;' class='btn btn-sm btn-primary btn-warning' href='javascript:void(0);'"
                    . "title='edit' onclick='edit_data(" . '"' . $value->kdpaslon . '"' . ")'>"
                    . "<i class='glyphicon glyphicon-pencil'></i> Edit</a>"
                    . "<a class='btn btn-sm btn-danger' href='javascript:void(0);' style='text-align:center; vertical-align: middle;'"
                    . "title='hapus' onclick='hapus_data(" . '"' . $value->kdpaslon . '"' . "," . '"' . $value->nmketua . '"' . " )'>"
                    . "<i class='glyphicon glyphicon-trash'></i> Hapus</a></td>";

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Paslon->countAll(),
            "recordsFiltered" => $this->Paslon->countFiltered(),
            "data" => $data
        );
        echo json_encode($output);
    }

    public function calon() {

        $login = $this->AdminIsLoggedIn();
        $data = array(
            'title' => 'OVCS Admin',
            'login' => $login
        );

        if ($login === TRUE) {
            $this->load->view('admin/paslon/view', $data);
        } else {
            $this->load->view('admin/login/view', $data);
        }
    }

    public function showCalon() {
        $data = $this->Paslon->show('paslon', '', '')->result();
        echo json_encode($data);
    }

    public function editCalon() {
        $id = $this->input->post('id');
        $data = $this->Paslon->show('kdpaslon', $id)->row();
//        $data->tgldaftar = ($data->tgldaftar == '0000-00-00') ? '' : $data->tgldaftar;
        echo json_encode($data);
    }

    public function updateCalon() {
        $kdpaslon = $this->input->post('id');
        $nmketua = $this->input->post('nmketua');
        $nmwakil = $this->input->post('nmwakil');
        $motto = $this->input->post('motto');
        $tgldaftar = $this->input->post('tgldaftar');
        $config = 'assets/upload/';
        $path = $config . $this->input->post('remove_photo');
        $this->_validasi();

        $data = array(
            'nmketua' => $nmketua,
            'nmwakil' => $nmwakil,
            'motto' => $motto,
            'tgldaftar' => $tgldaftar,
        );

        if ($this->input->post('remove_photo')) { // if remove photo checked
            if (file_exists($path) && $this->input->post('remove_photo')) {
                unlink(FCPATH . $path);
            }
              $data['foto'] = '';
        }

        if (!empty($_FILES['photo']['name'])) {
            $foto = $this->Paslon->show('kdpaslon', $kdpaslon)->row();
            if (file_exists($path) && $foto->foto) {
                $path = $config . $foto->foto;
                unlink(FCPATH . $path);
            }
            $upload = $this->_do_upload();
            $data['foto'] = $upload;
        }

        $this->Paslon->update($data, array('kdpaslon' => $kdpaslon));
        echo json_encode(array("status" => TRUE));
    }

    public function deleteCalon() {
        $id = $this->input->post('id');
        $config = 'assets/upload/';
        $path = "";
        $hapus = $this->Paslon->show('kdpaslon', $id)->row();

        if ($hapus->foto != "") {
            $path = $config . $hapus->foto;
        }
        if (file_exists($path)) {
            unlink(FCPATH . $path);
        }
        $this->Paslon->hapus('kdpaslon', $id);
        echo json_encode(array("status" => TRUE));
    }

    public function bulkDeleteCalon() {
        $daftarHapus = $this->input->post('id');
        $config = 'assets/upload/';
        $path = "";
        foreach ($daftarHapus as $id) {
            $hapus = $this->Paslon->show('kdpaslon', $id)->result();
            foreach ($hapus as $h) {

                if ($h->foto != "") {
                    $path = $config . $h->foto;
                }
                if (file_exists($path)) {
                    unlink(FCPATH . $path);
                }
                $this->Paslon->hapus('kdpaslon', $id);
            }
        }
        echo json_encode(array("status" => TRUE));
    }

    public function cekKode() {
        $kode = $this->input->post('id');
        $res = $this->Paslon->selectCount('kdpaslon', $kode)->result();
        if ($res[0]->cnt > 0) {
            echo '1';
        } else {
            echo '0';
        }
    }

    public function addCalon() {
        $id = $this->input->post('id');
        $nmketua = $this->input->post('nmketua');
        $nmwakil = $this->input->post('nmwakil');
        $motto = $this->input->post('motto');
        $tgldaftar = $this->input->post('tgldaftar');
        $this->_validasi();
        $data = array(
            'kdpaslon' => $id,
            'nmketua' => $nmketua,
            'nmwakil' => $nmwakil,
            'motto' => $motto,
            'tgldaftar' => $tgldaftar,
        );

        if (!empty($_FILES['photo']['name'])) {
            $upload = $this->_do_upload();
            $data['foto'] = $upload;
        }

        $this->Paslon->simpan($data);

        $tbhitung = array(
            'kdpaslon' => $id,
            'jmlsuara' => '0'
        );

        $this->Paslon->simpanHitung($tbhitung);

        echo json_encode(array("status" => TRUE));
    }

    private function _do_upload() {

        $nmketua = $this->input->post('nmketua');
        $nmwakil = $this->input->post('nmwakil');
        $d = date("y-m-d");
        $nmfile = $nmketua . "_" . $nmwakil . "_" . $d;

        $config['upload_path'] = 'assets/upload/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 10000;
        $config['max_width'] = 10000;
        $config['max_height'] = 10000;
//        $config['encrypt_name'] = TRUE;
        $config['file_name'] = $nmfile;
        $image_data = array();
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('photo')) {
            $data['inputerror'][] = 'photo';
            $data['error_string'][] = 'Upload error: ' . $this->upload->display_errors('', '');
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        } else {
            $image_data = $this->upload->data();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $image_data['full_path'];
            // $config['wm_text'] = 'bakaranproject';
//            $config['wm_type'] = 'text';
            // $config['wm_type'] = 'overlay';
            // $config['wm_overlay_path'] = 'assets/img/bp.png';
            //$config['wm_opacity'] = '50';
//            $config['wm_font_path'] = './system/fonts/texb.ttf';
//            $config['wm_font_size'] = '32';
//            $config['wm_font_color'] = '#707A7C';
            $config['wm_vrt_alignment'] = 'middle';
            $config['wm_hor_alignment'] = 'center';
            $config['wm_padding'] = '20';
            $this->load->library('image_lib', $config);
//            $this->image_lib->watermark();
            if (!$this->image_lib->watermark()) {
                $this->handle_error($this->image_lib->display_errors());
                $upload_path = "assets/upload/";
                $file = $upload_path . $image_data['file_name'];
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
        return $this->upload->data('file_name');
    }

    private function _validasi() {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
        $id = $this->input->post('id');
        $nmketua = $this->input->post('nmketua');
        $nmwakil = $this->input->post('nmwakil');
        $motto = $this->input->post('motto');
        $tgldaftar = $this->input->post('tgldaftar');

        if ($id == "") {
            $data['inputerror'][] = 'id';
            $data['error_string'][] = 'Kode / Nomor Urut Harus Diisi';
            $data['status'] = FALSE;
        }
        if ($nmketua == "") {
            $data['inputerror'][] = 'nmketua';
            $data['error_string'][] = 'Nama Ketua Harus Diisi';
            $data['status'] = FALSE;
        }
        if ($nmwakil == "") {
            $data['inputerror'][] = 'nmwakil';
            $data['error_string'][] = 'Nama Wakil Ketua Harus Diisi';
            $data['status'] = FALSE;
        }
        if ($motto == "") {
            $data['inputerror'][] = 'motto';
            $data['error_string'][] = 'Motto Harus Diisi';
            $data['status'] = FALSE;
        }
        if ($tgldaftar == "") {
            $data['inputerror'][] = 'tgldaftar';
            $data['error_string'][] = 'Tanggal daftar Harus Diisi';
            $data['status'] = FALSE;
        }
        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }

    function exportCSV() {
        $data = $this->Paslon->show('', '');
        $cek = $data->num_rows();
        if ($cek > 0) {
            $query = $data->result_array();
            $delimiter = ",";
            $filename = "paslon_" . date('Y-m-d') . ".csv";

            $f = fopen('php://memory', 'w');

            $fields = array('Kode', 'Nama Ketua', 'Nama Wakil', 'Motto', 'Tanggal Daftar', 'Foto');
            fputcsv($f, $fields, $delimiter);
            foreach ($query as $row) {
                $kode = $row['kdpaslon'];
                if ($kode < 10) {
                    $kode = "'" . $kode;
                }
                $lineData = array($kode, $row['nmketua'], $row['nmwakil'], $row['motto'], $row['tgldaftar'], $row['foto']);
                fputcsv($f, $lineData, $delimiter);
            }
            fseek($f, 0);
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="' . $filename . '";');
            fpassthru($f);
        }
        exit;
    }

    function exportToEXCEL() {
        $fileName = "paslon";
        $query = $this->Paslon->show('', '');
        $this->exportEXCEL($fileName, $query);
    }

    function exportToPDF() {
        $this->load->helper('tanggal');
        $data['title'] = 'Pasangan Calon';
        $data['tgl'] = hariTanggalBulanTahun();
        $data['paslon'] = $this->Paslon->show('', '')->result();
        $view = "admin/cetak/paslonPDF";
        $this->load->view($view, $data);
        $this->exportPDFL($view, $data);
    }

    function printData() {
        $data['title'] = 'Pasangan Calon';
        $data['paslon'] = $this->Paslon->show('', '')->result();
        $data['tgl'] = $this->tgl_indo(date('d-m-Y'));

        $this->load->view('admin/cetak/paslonCetak', $data);
    }

    function downloadExcel() {
        $query = $this->Paslon->show('', '')->result();
        $this->load->library('PHPExcel');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(17);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);

        $objPHPExcel->getActiveSheet()->getStyle(1)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle(2)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle(3)->getFont()->setBold(true);

        $header = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => 'FF0000'),
                'name' => 'Courier new'
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle("A1:F2")
                ->applyFromArray($header)
                ->getFont()->setSize(16);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:F2');
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Data Pasangan Calon Ketua Bakaran Project')
                ->setCellValue('A3', 'No')
                ->setCellValue('B3', 'Kode Calon')
                ->setCellValue('C3', 'Nama Ketua')
                ->setCellValue('D3', 'Nama Wakil')
                ->setCellValue('E3', 'Motto')
                ->setCellValue('F3', 'Tanggal Daftar');

        $ex = $objPHPExcel->setActiveSheetIndex(0);
        $no = 1;
        $counter = 4;
        foreach ($query as $row):
            $kode = $row->kdpaslon;
            if ($kode < 10) {
                $kode = "'" . $kode;
            }
            $ex->setCellValue('A' . $counter, $no++);
            $ex->setCellValue('B' . $counter, $kode);
            $ex->setCellValue('C' . $counter, $row->nmketua);
            $ex->setCellValue('D' . $counter, $row->nmwakil);
            $ex->setCellValue('E' . $counter, $row->motto);
            $ex->setCellValue('F' . $counter, $row->tgldaftar);

            $counter = $counter + 1;
        endforeach;

        $objPHPExcel->getProperties()->setCreator("Yoga Willy")
                ->setLastModifiedBy("Yoga Willy")
                ->setTitle("Export To EXCEL")
                ->setSubject("Export To Excel")
                ->setDescription("Office 2007 XLS, XLSX")
                ->setKeywords("office 2007 openxml php")
                ->setCategory("Excel");
        $objPHPExcel->getActiveSheet()->setTitle('Data Calon');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Last-Modified:' . gmdate("D, d M Y H:i:s") . 'GMT');
        header('Chace-Control: no-store, no-cache, must-revalation');
        header('Chace-Control: post-check=0, pre-check=0', FALSE);
        header('Pragma: no-cache');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="DataPasanganCalon' . date('Ymd') . '.xlsx"');

        $objWriter->save('php://output');
    }

    public function importFromCSV() {
        $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if (!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)) {
            if (is_uploaded_file($_FILES['file']['tmp_name'])) {
                $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                fgetcsv($csvFile);

                while (($line = fgetcsv($csvFile)) !== FALSE) {
                    $query = $this->Paslon->custom("SELECT kdpaslon FROM paslon WHERE kdpaslon = '" . $line[0] . "'")->result();
                    if (count($query) > 0) {
                        $data = array(
                            'nmketua' => $line[1],
                            'nmwakil' => $line[2],
                            'motto' => $line[3],
                            'tgldaftar' => $line[4],
                        );
                        $this->Paslon->update($data, array('kdpaslon' => $line[0]));
                    } else {

                        $data = array(
                            'kdpaslon' => $line[0],
                            'nmketua' => $line[1],
                            'nmwakil' => $line[2],
                            'motto' => $line[3],
                            'tgldaftar' => $line[4],
                        );
                        $this->Paslon->simpan($data);
                        $tbhitung = array(
                            'kdpaslon' => $line[0],
                            'jmlsuara' => '0'
                        );

                        $this->Paslon->simpanHitung($tbhitung);
                    }
                }
                fclose($csvFile);
            }
        }
        echo json_encode(array("status" => TRUE));
    }

    function importFromExcel() {
        $this->load->library('PHPExcel');
        $this->load->library('upload');
        $fileName = time() . $_FILES['file']['name'];
//        $excelMimes = array('text/x-comma-separated-values|text/comma-separated-values|application/octet-stream|application/vnd.ms-excel|application/excel|application/vnd.msexcel');
        $config['upload_path'] = 'assets/upload/fileExcel/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = "xls|xlsx";
        $config['max_size'] = 10000;
//

        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file')) {
            $data['inputerror'][] = 'file';
            $data['error_string'][] = 'Upload error: ' . $this->upload->display_errors(' ', ' ');
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        } else {
            $media = $this->upload->data();
            $inputFileName = 'assets/upload/fileExcel/' . $media['file_name'];
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                $data['inputerror'][] = 'file';
                $data['error_string'][] = 'Error loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                $data['status'] = FALSE;
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            for ($row = 2; $row <= $highestRow; $row++) {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $query = $this->Paslon->custom("SELECT kdpaslon FROM paslon WHERE kdpaslon = '" . $rowData[0][0] . "'")->result();
                if (count($query) > 0) {
                    $data = array(
                        "nmketua" => $rowData[0][1],
                        "nmwakil" => $rowData[0][2],
                        "motto" => $rowData[0][3],
                        "tgldaftar" => $rowData[0][4]
                    );
                    $this->Paslon->update($data, array('kdpaslon' => $rowData[0][0]));
                } else {
                    $data = array(
                        "kdpaslon" => $rowData[0][0],
                        "nmketua" => $rowData[0][1],
                        "nmwakil" => $rowData[0][2],
                        "motto" => $rowData[0][3],
                        "tgldaftar" => $rowData[0][4]
                    );

                    $this->Paslon->simpan($data);
                    $tbhitung = array(
                        'kdpaslon' => $rowData[0][0],
                        'jmlsuara' => '0'
                    );

                    $this->Paslon->simpanHitung($tbhitung);
                }

//                if (file_exists($inputFileName)) {
//                    unlink(FCPATH . $inputFileName);
//                }
                $this->load->helper("file");

                delete_files($media['file_path']);
            }
            echo json_encode(array("status" => TRUE));
        }
    }

}
