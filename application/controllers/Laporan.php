<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin
 *
 * @author Willy
 */
class Laporan extends Admin_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    function chart() {
        $login = $this->AdminIsLoggedIn();
        $data = array(
            'title' => 'OVCS Admin',
            'login' => $login
        );
        if ($login === TRUE) {
            $data['title'] = 'OVCS Chart';
            $data["paslon"] = $this->Laporan->custom("SELECT * FROM paslon LEFT JOIN hitung USING (kdpaslon) ORDER BY kdpaslon ASC")->result();
            $data["totalsuara"] = $this->Laporan->sum('jmlsuara', '', '')->row();
            $query = $this->Laporan->custom("SELECT idpemilih FROM pemilih")->result();
            $data["totalterdaftar"] = count($query);
            // die(json_encode($data));
            $this->load->view('admin/chart', $data);
            // $this->load->view('admin/chart_new', $data);
        } else {
            $this->load->view('admin/login/view', $data);
        }
    }

    function cetakLaporan() {
        $this->load->helper('tanggal');
        $login = $this->AdminIsLoggedIn();
        $data = array(
            'title' => 'OVCS Admin',
            'login' => $login
        );
        if ($login === TRUE) {
            $data['title'] = 'OVCS Chart';
            $data['tgl'] = hariTanggalBulanTahun();
            $data["paslon"] = $this->Laporan->custom("SELECT * FROM paslon LEFT JOIN hitung USING (kdpaslon) ORDER BY kdpaslon ASC")->result();
            $data["totalsuara"] = $this->Laporan->sum('jmlsuara', '', '')->row();
            $query = $this->Laporan->custom("SELECT idpemilih FROM pemilih")->result();
            $data["totalterdaftar"] = count($query);
            $view = "admin/cetak/perhitunganSuara";
            $this->load->view($view, $data);
            $this->exportPDFL($view, $data);
        } else {
            $this->load->view('admin/login/view', $data);
        }
    }

}
