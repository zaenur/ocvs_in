<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_Controller
 *
 * @author Willy
 */
class Admin_Controller extends MY_Controller {

    //put your code here
    protected function AdminIsLoggedIn() {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $AdminisLoggedIn = $this->session->userdata('admin_data');
        if (isset($AdminisLoggedIn)) {
            $inactive = 900;
            if (!isset($_SESSION['timeout'])) {
                $_SESSION['timeout'] = time() + $inactive;
            }

            $session_life = time() - $_SESSION['timeout'];

            if ($session_life > $inactive) {
                $this->session->set_flashdata('timeout', 'Your session has expired, please login again');
                session_destroy();
                return FALSE;
            }

            $_SESSION['timeout'] = time();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function kirimEmail($email, $subject, $message) {

        $this->load->library('email');
        stream_context_set_default([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE
            ]
        ]);

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.googlemail.com';
        $config['smtp_user'] = 'bakaranproject@gmail.com';
        $config['smtp_pass'] = 'InsyaAllah88';
        $config['smtp_port'] = '465';
        $config['smtp_crypto'] = 'ssl';
        $config['wordwrap'] = TRUE;
        // $config['wrapchars'] = 76;
        $config['charset'] = 'iso-8859-1';
        // $config['newline'] = "\r\n";
        // $config['crlf'] = "\r\n";
        $config['mailtype'] = 'html';
        // $config['validation'] = FALSE;

        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('bakaranproject@gmail.com', "Bakaran Project");
        $this->email->to($email);
//        $this->email->cc('willz.0811@gmail.com');
        $this->email->subject($subject);
        $this->email->message($message);
        // $this->email->send();
        // if ($this->email->send()) {
        //     echo json_encode(array("status" => TRUE));
        // } else {
        //     echo json_encode(array("status" => "Gagal Mengirim Email"));
        // }
        // echo $this->email->print_debugger();
    }

}
