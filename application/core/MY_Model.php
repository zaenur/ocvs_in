<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Model
 *
 * @author Willy
 */
class MY_Model extends CI_Model {

    //put your code here
    protected $table = '';

    function __construct() {
        parent::__construct();
        $this->load->database();
        if (!$this->table) {
            $this->table = str_replace('_model', '', get_class($this));
        }
    }

    function custom($q) {
        $query = $this->db->query($q);
        return $query;
    }

    function show($col, $condition) {
        if ($col === "" || $condition === "") {
            $query = $this->db->get($this->table);
            return $query;
        } else {
            $this->db->where($col, $condition);
            $query = $this->db->get($this->table);
            return $query;
        }
    }

    public function get() {
        return $this->db->get($this->table);
    }

    public function select($columns) {
        $this->db->select($columns);
        return $this;
    }

    public function where($column, $condition) {
        $this->db->where($column, $condition);
        return $this;
    }

    public function orLike($column, $condition) {
        $this->db->or_like($column, $condition);
        return $this;
    }

    public function join($table, $type = 'left') {
        $this->db->join($table, "$this->table.id_$table = $table.id_$table", $type);
        return $this;
    }

    public function orderBy($kolom, $order = 'asc') {
        $this->db->order_by($kolom, $order);
        return $this;
    }

    function sum($sum, $col, $kondisi) {
        if ($col == "" || $kondisi == "") {
            $this->db->select_sum($sum, 'total');
            $q = $this->db->get($this->table);
            return $q;
        } else {
            $this->db->select_sum($sum, 'total');
            $this->db->where($col, $kondisi);
            $q = $this->db->get($this->table);
            return $q;
        }
    }

    function selectCount($col, $condition) {
        $qry = "SELECT count(*) as cnt from " . $this->table . " where " . $col . " ='" . $condition . "';";
        return $this->db->query($qry);
    }

  

    public function simpan($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function simpanMultiple($data) {
        $this->db->insert_batch($this->table, $data);
    }

    public function update($data, $where) {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function hapus($col, $condition) {
        $this->db->where($col, $condition);
        $this->db->delete($this->table);
    }

}
