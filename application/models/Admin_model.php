<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin_model
 *
 * @author Willy
 */
class Admin_model extends MY_Model {

    //put your code here
    var $table = "admin";

    public function __construct() {
        parent::__construct();
    }

    function getRows($params = array()) {
        $this->db->select('*');
        $this->db->from($this->table);

        if (!empty($params['search']['keywords'])) {
            $this->db->like('username', $params['search']['keywords']);
        }

        if (!empty($params['search']['sortBy'])) {
            $this->db->order_by('username', $params['search']['sortBy']);
        } else {
            $this->db->order_by('kdadmin', 'desc');
        }

        if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit'], $params['start']);
        } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit']);
        }

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    public function insertToken($id) {
        $token = substr(sha1(rand()), 0, 30);
        $date = date('Y-m-d');

        $string = array(
            'token' => $token,
            'kdadmin' => $id,
            'created' => $date
        );
        $query = $this->db->insert_string('tokens', $string);
        $this->db->query($query);
        return $token . $id;
    }

    public function isTokenValid($token) {
        $tkn = substr($token, 0, 30);
        $uid = substr($token, 30);

        $q = $this->db->get_where('tokens', array(
            'tokens.token' => $tkn), 1);

        if ($this->db->affected_rows() > 0) {
            $row = $q->row();

            $created = $row->created;
            $createdTS = strtotime($created);
            $today = date('Y-m-d');
            $todayTS = strtotime($today);

            if ($createdTS != $todayTS) {
                return false;
            }

            $user_info = $this->getUserInfo($row->kdadmin);
            return $user_info;
        } else {
            return false;
        }
    }

    public function getUserInfo($id) {
        $q = $this->db->get_where('admin', array('kdadmin' => $id), 1);
        if ($this->db->affected_rows() > 0) {
            $row = $q->row();
            return $row;
        } else {
            error_log('no user found getUserInfo(' . $id . ')');
            return false;
        }
    }

}
