<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dataModel
 *
 * @author Willy
 */
class Pemilih_model extends MY_Model {

    //put your code here

    var $table = "pemilih";
    var $columnOrder = array(null, null, 'idpemilih', 'email', 'tgldaftar', 'status', null, null, null);
    var $columnSearch = array('idpemilih', 'email', 'passpemilih', 'tgldaftar', 'status',);
    var $order = array('idpemilih' => 'asc');

    public function __construct() {
        parent::__construct();
 
    }

    private function _getDataTablesQuery() {

        if ($this->input->post('idpemilih')) {
            $this->db->like('idpemilih', $this->input->post('idpemilih'));
        }
        if ($this->input->post('email')) {
            $this->db->like('email', $this->input->post('email'));
        }
        if ($this->input->post('tgldaftar')) {
            $this->db->like('tgldaftar', $this->input->post('tgldaftar'));
        }
        if ($this->input->post('status')) {
            $this->db->like('status', $this->input->post('status'));
        }

        $this->db->from($this->table);

        $i = 0;
        foreach ($this->columnSearch as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->columnSearch) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->columnOrder[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } elseif (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function getDataTables() {
        $this->_getDataTablesQuery();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
            $query = $this->db->get();
        }
        return $query->result();
    }

    function countFiltered() {
        $this->_getDataTablesQuery();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function countAll() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}
