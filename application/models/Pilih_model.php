<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dataModel
 *
 * @author Willy
 */
class Pilih_model extends MY_Model {

    //put your code here

    public function __construct() {
        parent::__construct();

    }

    function cekNIM($nim) {
        $qry = "SELECT count(*) as cnt from pemilih where idpemilih ='" . $nim . "';";
        return $this->db->query($qry);
    }

    function tampil($col, $condition) {
        if ($col === "" || $condition === "") {
            $query = $this->db->get("pemilih");
            return $query;
        } else {
            $this->db->where($col, $condition);
            $query = $this->db->get("pemilih");
            return $query;
        }
    }

    function tampilPaslon($col, $condition) {
        if ($col === "" || $condition === "") {
            $query = $this->db->get("paslon");
            return $query;
        } else {
            $this->db->where($col, $condition);
            $query = $this->db->get("paslon");
            return $query;
        }
    }

    function hitung($condition) {
            $this->db->where("kdpaslon", $condition);
            $query = $this->db->get("hitung");
            return $query;
    }

    function updateHitung($data, $where) {
        $this->db->update("hitung", $data, $where);
        return $this->db->affected_rows();
    }

    function updatePemilih($data, $where) {
        $this->db->update("pemilih", $data, $where);
        return $this->db->affected_rows();
    }

}
