<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of paslon_model
 *
 * @author Willy
 */
class Paslon_model extends MY_Model {

    //put your code here
    var $table = "paslon";
    var $columnOrder = array(null, null, 'kdpaslon', 'nmketua', 'nmwakil', 'motto', 'tgldaftar', 'foto', null);
    var $columnSearch = array('kdpaslon', 'nmketua', 'nmwakil', 'motto', 'tgldaftar', 'foto');
    var $order = array('kdpaslon' => 'asc');

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function _getDataTablesQuery() {

        if ($this->input->post('kode')) {
            $this->db->like('kdpaslon', $this->input->post('kode'));
        }
        if ($this->input->post('ketua')) {
            $this->db->like('nmketua', $this->input->post('ketua'));
        }
        if ($this->input->post('wakil')) {
            $this->db->like('nmwakil', $this->input->post('wakil'));
        }
        if ($this->input->post('moto')) {
            $this->db->like('motto', $this->input->post('moto'));
        }
        if ($this->input->post('tgl')) {
            $this->db->like('tgldaftar', $this->input->post('tgl'));
        }

        $this->db->from($this->table);

        $i = 0;
        foreach ($this->columnSearch as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->columnSearch) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->columnOrder[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } elseif (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function getDataTables() {
        $this->_getDataTablesQuery();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
            $query = $this->db->get();
        }
        return $query->result();
    }

    function countFiltered() {
        $this->_getDataTablesQuery();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function countAll() {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function simpanHitung($data) {
        $this->db->insert('hitung', $data);
        return $this->db->insert_id();
    }

}
