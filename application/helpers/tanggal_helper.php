<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of tanggal_helper
 *
 * @author Willy
 */
date_default_timezone_set('Asia/Jakarta');

function hari() {
    $hari = date("D");
    $hariArray = [
        'Minggu',
        'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        "Jum'at",
        'Sabtu'
    ];
    $hr = date('w', strtotime($hari));
    $hasil = $hariArray[$hr];
    return $hasil;
}

function tanggal() {
    $waktu = date("D");
    $tanggal = date('d', strtotime($waktu));
    return $tanggal;
}

function bulan() {
    $bulan = date("M");
    $bulanArray = [
        1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];

    $bl = date('n', strtotime($bulan));
    $hasil = $bulanArray[$bl];

    return $hasil;
}

function tahun() {
    $waktu = date("Y");
    $tahun = date('Y', strtotime($waktu));
    return $tahun;
}

function tanggalBulanTahun() {
    return tanggal() . " " . bulan() . " " . tahun();
}

function hariTanggalBulanTahun() {
    return hari() . ", " . tanggal() . " " . bulan() . " " . tahun();
}
