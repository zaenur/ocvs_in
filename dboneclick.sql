-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 25 Nov 2019 pada 07.09
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dboneclick`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `kdadmin` int(2) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `otoritas` varchar(20) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `lastLogin` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`kdadmin`, `username`, `password`, `otoritas`, `status`, `lastLogin`) VALUES
(11, 'yoga', '$2a$08$qyWjgV.Q7FZm6Iv9cQM1i.9UUFSW6uWtOe2geXQ7W5MTvpBsd4OMO', 'super admin', 'active', 'Kamis, 19 April 2018 11:00:19'),
(24, 'willz.0811@gmail.com', '$2y$10$FpX7zy47k.4Qv4rLJf7eAeyUKYpr2v/Cn75xBZz9vNu/EOckZcJeu', 'super admin', 'active', 'Selasa, 25 September 2018 9:01:27'),
(25, 'nandya@gmail.com', '$2a$08$dSyUHBLsZBKyvWgf0zsrYuD/cxUfQgvrvNL/R7mXlHzRwIbL0SQxa', 'super admin', 'active', 'Jum\'at, 06 Juli 2018 17:50:04'),
(26, 'zaenur.rochman98@gmail.com', '$2y$10$B0cUepCWe9tn55im0uHOmeZdA8iFOLRr90FsSEyXEILkQ7HCIlxfO', 'super admin', 'active', 'Senin, 25 November 2019 12:35:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hitung`
--

CREATE TABLE `hitung` (
  `id` int(11) NOT NULL,
  `kdpaslon` varchar(3) NOT NULL,
  `jmlsuara` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hitung`
--

INSERT INTO `hitung` (`id`, `kdpaslon`, `jmlsuara`) VALUES
(4, '01', 0),
(5, '02', 0),
(6, '03', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `paslon`
--

CREATE TABLE `paslon` (
  `kdpaslon` varchar(3) NOT NULL,
  `nmketua` varchar(100) DEFAULT NULL,
  `nmwakil` varchar(50) NOT NULL,
  `motto` text,
  `foto` text,
  `tgldaftar` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `paslon`
--

INSERT INTO `paslon` (`kdpaslon`, `nmketua`, `nmwakil`, `motto`, `foto`, `tgldaftar`) VALUES
('01', 'Muhammad Ali', ' ', '#WeAreFamily', 'Muhammad_Ali___19-11-22.jpeg', '22-November-2019'),
('02', 'Ihfan Adnan Maulana', ' ', '#WeAreFamily', 'Ihfan_Adnan_Maulana___19-11-22.jpeg', '22-November-2019'),
('03', 'Sofyan Wanadi', ' ', '#WeAreFamily', 'Sofyan_Wanadi___19-11-22.JPG', '22-November-2019');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemilih`
--

CREATE TABLE `pemilih` (
  `idpemilih` varchar(15) NOT NULL,
  `email` text,
  `passpemilih` varchar(50) NOT NULL,
  `tglDaftar` varchar(25) DEFAULT NULL,
  `status` varchar(20) DEFAULT 'bm'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pemilih`
--

INSERT INTO `pemilih` (`idpemilih`, `email`, `passpemilih`, `tglDaftar`, `status`) VALUES
('16.11.0062', 'rmaidak@gmail.com', 'FrRBl', '22 - November - 2019', 'bm'),
('16.11.0068', 'zaenur.rochman98@gmail.com', 'di3Wh', '15-April-2018', 'bm'),
('16.11.0070', 'yahyadani01@gmail.com', 'w4IWP', '22 - November - 2019', 'bm'),
('16.11.0137', 'untungnurkhifni@gmail.com', 'jlQd7', '25-November-2019', 'bm'),
('16.11.0180', 'triannurizkillah@gmail.com', '1JNB5', '25-November-2019', 'bm'),
('16.11.0254', 'rafly.firdausy@gmail.com', 'dqmrA', '20-November-2019', 'bm'),
('16.12.0109', 'kusumawrd13@gmail.com', 'lgjZ6', '25 - November - 2019', 'bm'),
('17.11.0001', 'deflofencing@gmail.com', 't5Eaj', '22 - November - 2019', 'bm'),
('17.11.0059', 'gzsony2821@gmail.com', 'Bncwc', '25 - November - 2019', 'bm'),
('17.11.0063', 'Atanalyaqin@gmail.com', 'lvx6T', '22 - November - 2019', 'bm'),
('17.11.0093', 'Zidaks14@gmail.com', '3szts', '22 - November - 2019', 'bm'),
('17.11.0116', 'ammaryanto99@gmail.com', '9vnx1', '25-November-2019', 'bm'),
('17.11.0166', 'ferdinandnyoman@gmail.com', 'Dcjjc', '22 - November - 2019', 'bm'),
('17.11.0169', 'rahmathidayatf@gmail.com', 'krRSK', '22 - November - 2019', 'bm'),
('17.11.0209', 'muflihdzulfian22@gmail.com', 'spx3d', '25-November-2019', 'bm'),
('17.11.0273', 'wawamaisa7@gmail.com', 'KYMCi', '22 - November - 2019', 'bm'),
('17.12.0027', 'destysautami@gmail.com', '78CtO', '25-November-2019', 'bm'),
('17.12.0059', 'nadillapu@gmail.com', 'zEOsr', '25-November-2019', 'bm'),
('17.12.0092', 'Deadraivankanailufar14@gmail.com', 'vuvyl', '22 - November - 2019', 'bm'),
('17.12.0116', 'elfariani52@gmail.com', '4SN1C', '22-November-2019', 'bm'),
('17.12.0140', 'impbarca046@gmail.com', '2fTpv', '22 - November - 2019', 'bm'),
('18.11.0004', 'dwichan@outlook.com', 'csYf8', '22 - November - 2019', 'bm'),
('18.11.0062', 'meliniaindah195@gmail.com', 'rwa14', '25-November-2019', 'bm'),
('18.11.0072', 'luckyrafi13@gmail.com', 'Otdbo', '22 - November - 2019', 'bm'),
('18.11.0115', 'cindviadika@gmail.com', '9qOkt', '25-November-2019', 'bm'),
('18.11.0116', 'ilyaoppo63@gmail.com', 'DhmeC', '25 - November - 2019', 'bm'),
('18.11.0120', 'sofyanwanaditi18c@gmail.com', 'Tbisl', '22 - November - 2019', 'bm'),
('18.11.0121', 'Bellatyaranf@gmail.com', 't8kMq', '25-November-2019', 'bm'),
('18.11.0127', 'akhyarmaulana82@gmail.com', 'CucaS', '25-November-2019', 'bm'),
('18.11.0132', 'trisnam83@gmail.com', 'cVpjr', '22 - November - 2019', 'bm'),
('18.11.0138', 'agnisnurafaz@gmail.com', 'dJien', '22 - November - 2019', 'bm'),
('18.11.0146', 'tandiirawan454@gmail.com', 'OCRpA', '22 - November - 2019', 'bm'),
('18.11.0150', 'akbarrozaq691@gmail.com', '8r5oc', '22 - November - 2019', 'bm'),
('18.11.0190', 'wicaknisa11@gmail.com', '2gOV8', '22 - November - 2019', 'bm'),
('18.11.0244', 'muhali.ma88@gmail.com', 'kt9Zf', '25 - November - 2019', 'bm'),
('18.11.0253', 'tegarpenemuan@gmail.com', 'yq384', '22 - November - 2019', 'bm'),
('18.11.0259', 'melaniahelani11@gmail.com', 'nm6Jj', '22 - November - 2019', 'bm'),
('18.12.0027', 'bellapujiastuti0@gmail.com', 't4SBm', '25-November-2019', 'bm'),
('18.12.0047', 'finaerf1004@gmail.com', 'DciDd', '22 - November - 2019', 'bm'),
('18.12.0092', 'isfatinfazrin20@gmail.com', 'd4zza', '25 - November - 2019', 'bm'),
('18.12.0112', 'dwiarindawisnuw@gmail.com', 'Cu5qu', '25 - November - 2019', 'bm'),
('18.12.0138', 'mdmynt18@gmail.com', 'yM8JT', '25-November-2019', 'bm'),
('18.12.0148', 'trisucidesiana@gmail.com', 'nVXVw', '25-November-2019', 'bm'),
('18.12.0150', 'gilang0399@gmail.com', 'HHih7', '25-November-2019', 'bm'),
('18.12.0151', 'aryaagustadaffa@gmail.com', 'kyigG', '25-November-2019', 'bm'),
('19.SA.1002', 'fadilray07@gmail.com ', 'A4CMo', '25-November-2019', 'bm'),
('19.SA.1004', 'gamiatsugani2000@gmail.com', 'ksCCo', '22 - November - 2019', 'bm'),
('19.SA.1005', 'lh400190@gmail.com', 'LLZIX', '22-November-2019', 'bm'),
('19.SA.1006', 'aancimot5@gmail.com', '5qPsV', '25 - November - 2019', 'bm'),
('19.SA.1009', 'rizalputra725@gmail.com', '7oO1e', '22-November-2019', 'bm'),
('19.SA.1010', 'Suryaramadhanb312@gmail.com', 'ifYF4', '22 - November - 2019', 'bm'),
('19.SA.1011', 'anantya4995@gmail.com', 'aQxk3', '22 - November - 2019', 'bm'),
('19.SA.1025', 'maulanawahyudi701@gmail.com', '96hKS', '25 - November - 2019', 'bm'),
('19.SA.1032', 'atinseptian6@gmail.com', 'h68Y0', '22 - November - 2019', 'bm'),
('19.SA.1037', 'Alfaahmad321@gmail.com', 'JuWsI', '25-November-2019', 'bm'),
('19.SA.1045', 'dinifira1208@gmail.com', 'ORslS', '22 - November - 2019', 'bm'),
('19.SA.1049', 'agngramdhan@gmail.com', 'yojoG', '22 - November - 2019', 'bm'),
('19.SA.1062', 'deni.mugi14@gmail.com', 'Tp5zT', '22 - November - 2019', 'bm'),
('19.SA.1064', 'nawafrizki2001@gmail.com', 'XH4vw', '22 - November - 2019', 'bm'),
('19.SA.1075', 'hanikjmt82@gmail.com', '6Z6aa', '25-November-2019', 'bm'),
('19.SA.1077', 'jerremia.bryan777@gmail.com', '9JdiY', '25-November-2019', 'bm'),
('19.SA.1078', 'SahasAlvarez15@gmail.com', 'WKOiw', '22 - November - 2019', 'bm'),
('19.SA.1090', 'ganangsapuro38@gmail.com', 'oxYkD', '25 - November - 2019', 'bm'),
('19.SA.1091', 'wahyudsx@gmail.com', '2wcRv', '22 - November - 2019', 'bm'),
('19.SA.1100', 'rluthfi320@gmail.com', 'Wo5qo', '25-November-2019', 'bm'),
('19.SA.1101', 'rofiqulumma@gmail.com', 'wXRQk', '22 - November - 2019', 'bm'),
('19.SA.1104', 'fauzanhelmif22@gmail.com', '37wFh', '25 - November - 2019', 'bm'),
('19.SA.1107', 'Bagustriatmojo041@gmail.com', 'Ft0g4', '25-November-2019', 'bm'),
('19.SA.1110', 'Melati15022001@gmail.com', 'OzGpM', '25-November-2019', 'bm'),
('19.SA.1113', 'oktalianalst@gmail.com', '5jKWH', '22 - November - 2019', 'bm'),
('19.SA.1114', 'okyevanf@gmail.com ', 'Rd2Tw', '25 - November - 2019', 'bm'),
('19.SA.1134', 'alfiandaffa16@gmail.com', '5qnLN', '22 - November - 2019', 'bm'),
('19.SA.1143', 'ariqoh256@gmail.com', 'x38T6', '22 - November - 2019', 'bm'),
('19.SA.1145', 'alisusanto806@gmail.com', 'yIEj1', '22 - November - 2019', 'bm'),
('19.SA.1146', 'wellyaditama12@gmail.com', 'QMDgD', '22 - November - 2019', 'bm'),
('19.SA.1148', 'noviaasriromadhoni29@gmail.com', '0Ihn7', '22 - November - 2019', 'bm'),
('19.SA.1155', 'anggonoesanur@gmail.com', 'i1k9M', '22 - November - 2019', 'bm'),
('19.SA.1161', 'm.afrizalabiansyah45@gmail.com', 'XP0lr', '22 - November - 2019', 'bm'),
('19.SA.1162', 'anisaayuhalimah@gmail.com', 'jt8wF', '25-November-2019', 'bm'),
('19.SA.1163', 'farisfakhrizal9@gmail.com', 'U67wJ', '22 - November - 2019', 'bm'),
('19.SA.1167', 'Rahmatadam40@gmail.com', 'rjnU1', '25-November-2019', 'bm'),
('19.SA.1168', 'adenbahtiar36@gmail.com', 'O3F2n', '22 - November - 2019', 'bm'),
('19.SA.1177', 'rizki.meilia07@gmail.com', 'sJLSh', '25 - November - 2019', 'bm'),
('19.SA.1184', 'thomas.pramudya0@gmail.com', '3HjxB', '25-November-2019', 'bm'),
('19.SA.1192', 'rizalsidik231001@gmail.com', 'ZIiXF', '22 - November - 2019', 'bm'),
('19.SA.1200', 'pmahera6@gmail.com', 'kp4BJ', '22 - November - 2019', 'bm'),
('19.SA.1203', 'uuswatunhasanahh123@gmail.com', 'vFQJe', '25 - November - 2019', 'bm'),
('19.SA.1205', 'muhmaruf354@gmail.com', 'J6XWA', '25-November-2019', 'bm'),
('19.SA.1206 ', 'elfandicahyapradana@gmail.com', 'JTgW9', '25-November-2019', 'bm'),
('19.SA.1208', 'gilangfahmi20@gmail.com', 'UGg44', '22 - November - 2019', 'bm'),
('19.SA.1215', 'tantyowidyatmoko559@gmail.com', 'KuIYR', '25-November-2019', 'bm'),
('19.SA.1231', 'dutaaditya95@gmail.com', '6y8ox', '22 - November - 2019', 'bm'),
('19.SA.1232', 'Arifin290900@gmail.com', '7cZFT', '22 - November - 2019', 'bm'),
('19.SA.1234', 'alloddadwy@gmail.com', 'L8Eqq', '22-November-2019', 'bm'),
('19.SA.1239', 'm.syahri@outlook.com', 'fifMN', '22 - November - 2019', 'bm'),
('19.SA.1243', 'masingru2x@gmail.com', 'Ldut8', '22 - November - 2019', 'bm'),
('19.SA.1244 ', 'suryaatmaja644@gmail.com ', 'sT1Rc', '25 - November - 2019', 'bm'),
('19.SA.1258', 'ginacu.gc@gmail.com', 'YrLqL', '22 - November - 2019', 'bm'),
('19.SA.1260', 'naelibbs@gmail.com', 'zVLCI', '25-November-2019', 'bm'),
('19.SA.1268', 'gilangesaputra87@gmail.com', 'hvUwl', '22 - November - 2019', 'bm'),
('19.SA.1270', 'arifmunandar.che@gmail.com', 'NzctX', '22 - November - 2019', 'bm'),
('19.SA.1276', 'suyono3105@gmail.com', 'swYxE', '25-November-2019', 'bm'),
('19.SA.1279', 'kotamangga128980@gmail.com', '83CMc', '22 - November - 2019', 'bm'),
('19.SA.1280', 'Bondansigitpurnomo@gmail.com', 'AX8MD', '22 - November - 2019', 'bm'),
('19.SA.1668', 'rifqifajarzain@gmail.com', 'pM2X5', '22 - November - 2019', 'bm'),
('19.SA.2001', 'sefifiansah03@gmail.com', '5LyUO', '25-November-2019', 'bm'),
('19.SA.2002', 'nulnoel08@gmail.com', 'ACqMf', '22 - November - 2019', 'bm'),
('19.SA.2003', 'rifyadinda@gmail.com', 'a02eA', '25-November-2019', 'bm'),
('19.SA.2006', 'annisahandayani306@gmail.com', 'I2yz5', '22 - November - 2019', 'bm'),
('19.SA.2025', 'slametfajarch@gmail.com', 'sZmow', '25-November-2019', 'bm'),
('19.SA.2031', 'willyardow26@gmail.com', 'eicJ9', '22 - November - 2019', 'bm'),
('19.SA.2032', 'ryotase12@gmail.com', 'fnfAg', '25-November-2019', 'bm'),
('19.SA.2038', 'rismanovanda05@gmail.com', 'pXQeY', '25-November-2019', 'bm'),
('19.SA.2039', 'Nuragustina57@gmail.com', 'KJAbD', '22 - November - 2019', 'bm'),
('19.SA.2044', 'refitaandriyani17@gmail.com', 'QMYxU', '25-November-2019', 'bm'),
('19.SA.2047', 'dewifortuuunaaa@gmail.com', 'hl1Zc', '25-November-2019', 'bm'),
('19.SA.2053', 'fafmimusyafa@gmail.com', 'pNnkR', '22 - November - 2019', 'bm'),
('19.SA.2054', 'erikaluthfiafifah@gmail.com', 'DCCVg', '22 - November - 2019', 'bm'),
('19.SA.2055', 'verenakristiana@gmail.com', 'WkhR0', '25 - November - 2019', 'bm'),
('19.SA.2062', 'nevitacahayar1@gmail.com', 'Vg3aQ', '22 - November - 2019', 'bm'),
('19.SA.2063', 'melidautami23@gmail.com', 'vaXNX', '22 - November - 2019', 'bm'),
('19.SA.2068', 'muflikhatun.siti@gmail.com', 'HnUQV', '22 - November - 2019', 'bm'),
('19.SA.2071', 'rahajengningrump59@gmail.com', 'VVFLy', '25-November-2019', 'bm'),
('19.SA.2072', 'nethatw3@gmail.com', 'tGgsT', '22 - November - 2019', 'bm'),
('19.SA.2074', 'nandanurisya@gmail.com', 'm06Ar', '22 - November - 2019', 'bm'),
('19.SA.2091', 'taltaweersa@gmail.com', 'jwUNB', '22 - November - 2019', 'bm'),
('19.SA.2094', 'Ajiy251@gmail.com', 'TSDA3', '22 - November - 2019', 'bm'),
('19.SA.2097', 'silviaanggunr@gmail.com', 'bizMp', '25-November-2019', 'bm'),
('19.SA.2116', 'salmaey2022@gmail.com', 'AIy57', '25-November-2019', 'bm'),
('19.SA.3020', 'kuuhaku13fa@gmail.com', 'm5rGw', '25 - November - 2019', 'bm'),
('19.SA.3032', 'rafliagumprasojo48@gmail.com', 'WLzd9', '22 - November - 2019', 'bm'),
('19.SA.3056', 'rizkhaadinda99@gmail.com', 'gWgyR', '25-November-2019', 'bm'),
('19.SA.3060', 'agistiaagistia491@gmail.com ', 'yPpJG', '25 - November - 2019', 'bm'),
('19.SA.3064', 'fendielyonramadhan@gmail.com', 'AgU5X', '25 - November - 2019', 'bm'),
('19.SA.3074', 'dwiayumutiara270@gmail.com', '7oZIH', '22 - November - 2019', 'bm'),
('19.SA.3089', 'Astitasabilla@gmail.com', 'SJELK', '22 - November - 2019', 'bm'),
('19.SB.1003', 'rizqiniategar28@gmail.com', 'ByR6p', '25 - November - 2019', 'bm'),
('19.SB.1004', 'ekariana37@gmail.com', 'Ua3yB', '22 - November - 2019', 'bm'),
('19.SB.1019', 'nadillaala@gmail.com', 'orI7V', '22 - November - 2019', 'bm'),
('19.SB.1021', 'nitaputri17413@gmail.com', 'gay4x', '25-November-2019', 'bm'),
('19.SB.1031', 'mhafain11@yahoo.com', 'oK5qs', '25-November-2019', 'bm'),
('19.SB.1044 ', 'Biassetya@gmail.com ', 'jBkLg', '25-November-2019', 'bm'),
('19.SB.2006', 'merliana945@gmail.com', 'SjOAs', '25-November-2019', 'bm'),
('19.SB.2019', 'Annisatlestari12@gmail.com', 'R3IMs', '22 - November - 2019', 'bm');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pilih`
--

CREATE TABLE `pilih` (
  `idpemilih` varchar(15) DEFAULT NULL,
  `waktu` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `kdadmin` int(2) NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`kdadmin`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `hitung`
--
ALTER TABLE `hitung`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hitung_ibfk_1` (`kdpaslon`);

--
-- Indexes for table `paslon`
--
ALTER TABLE `paslon`
  ADD PRIMARY KEY (`kdpaslon`);

--
-- Indexes for table `pemilih`
--
ALTER TABLE `pemilih`
  ADD PRIMARY KEY (`idpemilih`);

--
-- Indexes for table `pilih`
--
ALTER TABLE `pilih`
  ADD KEY `idpemilih` (`idpemilih`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `kdadmin` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `hitung`
--
ALTER TABLE `hitung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `hitung`
--
ALTER TABLE `hitung`
  ADD CONSTRAINT `FK_hitungpaslon` FOREIGN KEY (`kdpaslon`) REFERENCES `paslon` (`kdpaslon`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pilih`
--
ALTER TABLE `pilih`
  ADD CONSTRAINT `pilih_ibfk_1` FOREIGN KEY (`idpemilih`) REFERENCES `pemilih` (`idpemilih`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
